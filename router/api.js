const verifySignUp = require('./verifySignUp');
const authJwt = require('./verifyJwtToken');
var mysql = require('mysql');  
const env = require('../config/env.js');

module.exports = function(app) {

    const controller = require('../controller/controller.js');
	
    const stats = require('../controller/StatsController.js');
	const upcomings = require('../controller/UpcomingsController.js');
	const packages = require('../controller/PackagesController.js');
	const contactus = require('../controller/ContactUsController.js');
	const myprofile = require('../controller/UsersController.js');
	const mypackages = require('../controller/MyPackagesController.js');
	const instagrampost = require('../controller/InstagramPostController.js');
	const giveaway = require('../controller/GiveawayController.js');
	const winners = require('../controller/WinnersController.js');
	const payment = require('../controller/PaymentController.js');
	const speech = require('../controller/SpeechController.js');
	const OperationsController = require('../controller/OperationsController.js');
	const operations = new OperationsController();

	//Admin Controllers
	const users = require('../controller/Admin/UsersController.js');
	const sales = require('../controller/Admin/SalesController.js');

	app.all("/api/ck/*",function(req,res,next){
		var connection = mysql.createConnection({
			host: env.host,
			user: env.username,
			password: env.password,
			database: env.database
		});
		connection.connect(function(err) {
		  if(err){
			next(err);
			return;
		  } 
		  res.locals.connection = connection;
		  next();
		});
	  });
	
	app.get('/api/stats', stats.get);
	app.get('/api/upcomings', upcomings.get);
	app.get('/api/packages', packages.get);
	app.post('/api/contactus', contactus.post);
	app.get('/api/detail', giveaway.detail);
	app.get('/api/winners', winners.get);
	app.post('/api/payment', [authJwt.verifyToken], payment.post);
	app.post('/api/paymentcb', payment.postcb);
	app.get('/api/payment', payment.get);

	app.get("/api/ck/fetchcomments", [authJwt.verifyToken], operations.fetchComments);
	app.get("/api/ck/dogiveaway", [authJwt.verifyToken], operations.doGiveAway);
	app.get("/api/ck/randomcomments", [authJwt.verifyToken], operations.getRandomComments);
	app.post("/api/ck/winnerstatus", [authJwt.verifyToken], operations.setWinnerStatus);
	app.post("/api/ck/followcontrol", [authJwt.verifyToken], operations.setFollowControl);

	app.get('/api/sales', [authJwt.verifyToken], mypackages.get);

	app.get('/api/giveaway', [authJwt.verifyToken], giveaway.get);
	app.post('/api/giveaway', [authJwt.verifyToken], giveaway.post);
	app.put('/api/giveaway', [authJwt.verifyToken], giveaway.put);

	app.post('/api/speech', speech.post);

	app.post('/api/instapost/get', [authJwt.verifyToken], operations.getMediaInfo);

	app.post('/api/register', [verifySignUp.checkDuplicateUserNameOrEmail], controller.signup);
	app.post('/api/login', controller.signin);

	// Logged User Routing
	app.get('/api/users', [authJwt.verifyToken], myprofile.get);
	app.post('/api/myprofile', [authJwt.verifyToken], myprofile.post);

	// Admin Routing
	app.get('/api/admin/users', [authJwt.verifyTokenAdmin], users.get);
	app.get('/api/admin/sales', [authJwt.verifyTokenAdmin], sales.get);

	app.all("/api/ck/*",function(req,res,next){
		if(res.locals.connection){
		  res.locals.connection.end();
		}
		res.json(res.locals.response);
		res.end();
	});  
  
	app.all("/api/ck/*",function(err,req,res,next){
		if(res.locals.connection){
		  res.locals.connection.end();
		}
		console.log(err);
		res.status(500);
		res.json({
		  code : 500,
		  detail : err
		});
	});

	/*app.post('/api/auth/register', [verifySignUp.checkDuplicateUserNameOrEmail], controller.signup);
	
	app.post('/api/auth/signin', controller.signin);
	
	app.get('/api/test/user', [authJwt.verifyToken], controller.userContent);

	app.post('/api/auth/verify', [authJwt.verifyToken], controller.verify);
	app.post('/api/auth/verifycomplete', [authJwt.verifyToken], controller.verifyComplete);

	app.post('/api/user/update', [authJwt.verifyToken], users.post);

	app.post('/api/user/updateos', [authJwt.verifyToken], users.updateos);
	app.post('/api/user/updatedevice', [authJwt.verifyToken], users.updatedevice);
	
	app.get('/api/test/pm', [authJwt.verifyToken, authJwt.isPmOrAdmin], controller.managementBoard);
	
	app.get('/api/test/admin', [authJwt.verifyToken, authJwt.isAdmin], controller.adminBoard);

	app.get('/api/userjokers', [authJwt.verifyToken], userjokers.get);
	app.post('/api/userjokers', [authJwt.verifyToken], userjokers.update);

	app.get('/api/references', [authJwt.verifyToken], references.get);
	app.get('/api/offerwall', [authJwt.verifyToken], offerwall.get);
	app.get('/api/games', [authJwt.verifyToken], games.get);
	app.post('/api/newgame', [authJwt.verifyToken], games.post);

	//GameLap
	app.get('/api/gamelaps', [authJwt.verifyToken], gamelaps.get);
	app.post('/api/gamelap/card/update', [authJwt.verifyToken], gamelaps.updateCard);
	app.post('/api/gamelap/result/update', [authJwt.verifyToken], gamelaps.updateResult);
	app.post('/api/gamelap/lessthan', [authJwt.verifyToken], gamelaps.lessThan);
	app.post('/api/gamelap/greaterthan', [authJwt.verifyToken], gamelaps.greaterThan);
	app.post('/api/gamelap/getprize', [authJwt.verifyToken], gamelaps.getPrize);
	app.post('/api/gamelap/continuegame', [authJwt.verifyToken], gamelaps.continueGame);
	app.post('/api/gamelap/joker/use', [authJwt.verifyToken], gamelaps.useJoker);

	app.get('/api/paymentmethods', [authJwt.verifyToken], paymentmethods.get);
	app.get('/api/transactions', [authJwt.verifyToken], transactions.get);
	app.post('/api/transactions', [authJwt.verifyToken], transactions.post);
	app.get('/api/support', [authJwt.verifyToken], support.get);
	app.get('/api/users', [authJwt.verifyToken], users.get);
	app.get('/api/slider', slider.get);

	app.get('/api/postback/adgate', [authJwt.verifyToken], adgate.get);
	app.get('/api/postback/admob', [authJwt.verifyToken], admob.get);
	app.post('/api/postback/admob', [authJwt.verifyToken], admob.post);

	app.get('/api/offerwalllist', offerwalllist.get);*/



}