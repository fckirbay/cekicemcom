-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Anamakine: 127.0.0.1:3306
-- Üretim Zamanı: 01 Oca 2021, 15:27:41
-- Sunucu sürümü: 5.7.21
-- PHP Sürümü: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `cekicem`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `pricing`
--

DROP TABLE IF EXISTS `pricing`;
CREATE TABLE IF NOT EXISTS `pricing` (
  `p1` decimal(7,2) NOT NULL,
  `p2` decimal(7,2) NOT NULL,
  `p3` decimal(7,2) NOT NULL,
  `p4` decimal(7,2) NOT NULL,
  `p5` decimal(7,2) NOT NULL,
  `p6` decimal(7,2) NOT NULL,
  `p7` decimal(7,2) NOT NULL,
  `p8` decimal(7,2) NOT NULL,
  `p9` decimal(7,2) NOT NULL,
  `p10` decimal(7,2) NOT NULL,
  `p11` decimal(7,2) NOT NULL,
  `p12` decimal(7,2) NOT NULL,
  `p13` decimal(7,2) NOT NULL,
  `p14` decimal(7,2) NOT NULL,
  `p15` decimal(7,2) NOT NULL,
  `p16` decimal(7,2) NOT NULL,
  `p17` decimal(7,2) NOT NULL,
  `p18` decimal(7,2) NOT NULL,
  `p19` decimal(7,2) NOT NULL,
  `p20` decimal(7,2) NOT NULL,
  `p21` decimal(7,2) NOT NULL,
  `p22` decimal(7,2) NOT NULL,
  `p23` decimal(7,2) NOT NULL,
  `p24` decimal(7,2) NOT NULL,
  `p25` decimal(7,2) NOT NULL,
  `p26` decimal(7,2) NOT NULL,
  `p27` decimal(7,2) NOT NULL,
  `p28` decimal(7,2) NOT NULL,
  `p29` decimal(7,2) NOT NULL,
  `p30` decimal(7,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `pricing`
--

INSERT INTO `pricing` (`p1`, `p2`, `p3`, `p4`, `p5`, `p6`, `p7`, `p8`, `p9`, `p10`, `p11`, `p12`, `p13`, `p14`, `p15`, `p16`, `p17`, `p18`, `p19`, `p20`, `p21`, `p22`, `p23`, `p24`, `p25`, `p26`, `p27`, `p28`, `p29`, `p30`) VALUES
('9.99', '19.99', '29.99', '14.99', '39.99', '59.99', '19.99', '49.99', '74.99', '24.99', '69.99', '99.99', '29.99', '74.99', '124.99', '49.99', '129.99', '224.99', '99.99', '259.99', '449.99', '249.99', '699.99', '999.99', '699.99', '1999.99', '2999.99', '999.99', '2499.99', '3999.99');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `stats`
--

DROP TABLE IF EXISTS `stats`;
CREATE TABLE IF NOT EXISTS `stats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_cnt` int(11) NOT NULL,
  `raffle_cnt` int(11) NOT NULL,
  `winner_cnt` int(11) NOT NULL,
  `comment_cnt` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `stats`
--

INSERT INTO `stats` (`id`, `user_cnt`, `raffle_cnt`, `winner_cnt`, `comment_cnt`) VALUES
(1, 143, 32, 56, 1753765);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `upcomings`
--

DROP TABLE IF EXISTS `upcomings`;
CREATE TABLE IF NOT EXISTS `upcomings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `caption` text NOT NULL,
  `photo_url` varchar(2000) NOT NULL,
  `date` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `follower_cnt` int(11) NOT NULL DEFAULT '0',
  `comment_cnt` int(11) NOT NULL DEFAULT '0',
  `is_verified` char(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `upcomings`
--

INSERT INTO `upcomings` (`id`, `username`, `caption`, `photo_url`, `date`, `follower_cnt`, `comment_cnt`, `is_verified`) VALUES
(1, 'tuceliztos', 'Geldiiiii🥳 3 kişiye Iphone11🥳\r\n@ulkuhur ve @meltemkavdir takibe almanız yeterliii🥳\r\nBoool bol yorum yapabilirsiniz etiket yapabilirsiniz🥳\r\n1 Ocak’ta canlı yayınla açıklayacağım sonucu. Şans sizlerle olsun sizi seviyoruuum❤️', 'https://instagram.fesb3-2.fna.fbcdn.net/v/t51.2885-15/sh0.08/e35/p640x640/129775866_406054987186667_6882921832048350477_n.jpg?_nc_ht=instagram.fesb3-2.fna.fbcdn.net&_nc_cat=1&_nc_ohc=B0oEBbJnO4YAX-s49Xn&tp=1&oh=fda079334f02a6ee8ea5db674e0aabd3&oe=60148C1C', '2020-02-01 00:00:00', 1000000, 43999, '0'),
(2, 'damlaaltun', 'hazır mıyııııııızzzzzzzz?!?!?! 😍\r\naranızdan tam 11 kişi buradan istediği ürünü alacak!\r\nKatılmak için beni @damlaaltun ve @ugursengulx takip edip, “istediğiniz ürünü” yazıp birkaç arkadaşınızı taglemeniz gerekiyor❤️ istediğiniz ürüne istediğiniz kadar yorum bırakabilirsiniz! Yılbaşında açıklayacağız🧨\r\n\r\nÜrünler:\r\nApple Macbook Air\r\nApple Iphone 11 128GB\r\nApple AirPods 2.Nesil\r\nAlexander McQueen ayakkabı (değişim kartı var 38.5 numara)\r\nValentino Kartlık\r\nYSL Libre & Black Opium Parfüm\r\nVersace Eros Parfüm Seti\r\nSephora (bütün kutular)\r\nNars (çantası ile birlikte)\r\nClinique Yılbaşı Seti\r\nLoreal Seti', 'https://instagram.fadb3-2.fna.fbcdn.net/v/t51.2885-15/sh0.08/e35/p640x640/130910481_829714824530250_3840637163292997007_n.jpg?_nc_ht=instagram.fadb3-2.fna.fbcdn.net&_nc_cat=1&_nc_ohc=BY1rB-reVbgAX8mTpyG&tp=1&oh=16bcc24ad2c9e50a781f2bbbe0dd8da7&oe=6014C21A', '2020-02-01 00:00:00', 123412, 1234, '1'),
(3, 'ceernkaya', '💘YILBAŞI ÇEKİLİŞİİİİİ💘 HEM DE ÇEKİLİŞ GİBİ ÇEKİLİŞ “IPHONE 12 PRO!!!” ÇIKAR ÇIKMAZ ALDIK SİZE😍😍😍❤️\r\nKurallar:\r\n- Beni @ceernkaya ve @lale.d takip etmek\r\n- Bu fotoğrafın altına dilediğiniz kadar arkadaşınızı etiketlemek, ne kadar yorum o kadar şans isterseniz 1 milyon kere etiketleyin 😂\r\n- Bu görseli ve bundan sonraki Lale’nin de benim de paylaşacağımız görselleri beğenmek🙋🏻‍♀️\r\n\r\nHerkese bol şans diliyoruz 😍🍀❤️ Kim kazanırsa bu sefer harbiden şeytanın bacağını kırdı demektir😍\r\n\r\nÇekiliş kazananı 1 OCAK günü açıklıyorummm❤️🙋🏻‍♀️\r\n#çekiliş #çekilişvar', 'https://instagram.fist1-2.fna.fbcdn.net/v/t51.2885-15/sh0.08/e35/p640x640/129670306_209868944059830_8229089558011799764_n.jpg?_nc_ht=instagram.fist1-2.fna.fbcdn.net&_nc_cat=1&_nc_ohc=H17f3OKCR7IAX-uIXUy&tp=1&oh=d1e6ffe5df99728d3a993635b902acff&oe=6015CE9D', '2020-02-05 00:00:00', 124, 12, '0'),
(4, 'oktarozge', 'DEV yılbaşı hediyesi 🎄🎁🎉\r\nHesabımı açtığımdan beri yaptığım ennnn büyük çekiliş 🤩🙈\r\nBen bile inanılmaz heyecanlıyım.\r\nÇok düşündüm, size sordum, sonunda böyle olmasına karar verdim.\r\nBir kişiye taaamm 10.000 TL’lik evet yanlış okumadınız 🙈 tam 10.000 TL’lik istediği markalardan çek hediye ediyorum ( tutarı istediğiniz kadar markaya bölebilirsiniz, aklınıza gelen her ama her markayı seçebilirsiniz👍🏻😃 )\r\nKazanan kişi ile iletişime geçip, istediği markalara ne kadarlık çekler ayırmak istediğini soracağım, sonrasında gift kartlarını toparlayıp adresine kargolayacağım.\r\nBöylelikle herkese, her isteğe hitap eden bir hediye oldu diye düşünüyorum ☺️\r\nBu DEV ÇEKİLİŞE KATILMAK İÇİN\r\n* Bu fotoğrafı beğenin\r\n* Beni @oktarozge takip edin\r\n* İstediğiniz kadar arkadaşınızı etiketleyin\r\n* Ne kadar çok etiket o kadar çok şans!!🤩\r\nÇekiliş sonucunu 1 OCAK tarihinde bu postta ve storylerde açıklayacağım!\r\nBu DEV ÇEKİLİŞİ arkadaşlarınızla, ailenizle paylaşmayı unutmayın!\r\nHerkese bol bol bol şans 🍀\r\n#çekiliş #cekilis #çekilişvar #cekilisvar', 'https://instagram.fist1-2.fna.fbcdn.net/v/t51.2885-15/sh0.08/e35/p640x640/129603363_380432769694987_8419103047702421078_n.jpg?_nc_ht=instagram.fist1-2.fna.fbcdn.net&_nc_cat=107&_nc_ohc=KVm-bPg4uxMAX-xUooa&tp=1&oh=3d873e3bd0f92d1f6476f49592b9eed5&oe=60135E2D', '2020-02-05 00:00:00', 3324523, 34523, '0');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(200) NOT NULL,
  `name_surname` varchar(200) NOT NULL,
  `phone_number` varchar(15) DEFAULT NULL,
  `password` varchar(512) NOT NULL,
  `lang` varchar(3) NOT NULL DEFAULT 'TR',
  `verification` char(1) NOT NULL DEFAULT '0',
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `users`
--

INSERT INTO `users` (`id`, `email`, `name_surname`, `phone_number`, `password`, `lang`, `verification`, `createdAt`, `updatedAt`) VALUES
(11, 'bekirozyurt@gmail.com', 'Bekir Ozyurt', NULL, '$2a$08$v29rScNvo4U801n.VbgGWO.P8e3suJFtmzHKZpgR7GYb3iVFqJ2ma', 'TR', '0', '2020-12-30 22:05:36', '2020-12-30 22:05:36'),
(10, 'firatkirbay@gmail.com', 'Fırat Kırbay', NULL, '$2a$08$v29rScNvo4U801n.VbgGWO.P8e3suJFtmzHKZpgR7GYb3iVFqJ2ma', 'TR', '0', '2020-12-30 22:05:36', '2020-12-30 22:05:36');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
