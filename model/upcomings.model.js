module.exports = (sequelize, Sequelize) => {
	const Upcomings = sequelize.define('upcomings', {
	  username: {
		  type: Sequelize.STRING
	  },
	  caption: {
		  type: Sequelize.STRING
	  },
	  photo_url: {
		  type: Sequelize.STRING
      },
      date: {
        type: Sequelize.STRING
      },
	  follower_cnt: {
		  type: Sequelize.STRING
	  },
	  comment_cnt: {
		  type: Sequelize.STRING
	  },
	  is_verified: {
		  type: Sequelize.STRING
	  }
	}, {
	    freezeTableName: true
	}, {
	    timestamps: false
	});
	
	return Upcomings;
}