module.exports = (sequelize, Sequelize) => {
	const Packages = sequelize.define('packages', {
	  id: {
          type: Sequelize.STRING,
          primaryKey: true,
          autoIncrement: true
	  },
	  name: {
		  type: Sequelize.STRING
	  },
	  description: {
		type: Sequelize.STRING
	  },
	  price: {
		  type: Sequelize.STRING
      },
      post_limit: {
        type: Sequelize.STRING
      }
	}, {
	    freezeTableName: true
	}, {
	    timestamps: false
	});
	
	return Packages;
}