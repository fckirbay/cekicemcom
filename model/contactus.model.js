module.exports = (sequelize, Sequelize) => {
	const ContactUs = sequelize.define('contact_us', {
	  id: {
          type: Sequelize.STRING,
          primaryKey: true,
          autoIncrement: true
	  },
	  email: {
		  type: Sequelize.STRING
	  },
	  subject: {
		  type: Sequelize.STRING
	  },
	  message: {
		  type: Sequelize.STRING
	  },
	  replied: {
		  type: Sequelize.STRING
	  },
	  createdAt: {
		  type: Sequelize.DATE
	  },
	  updatedAt: {
		  type: Sequelize.DATE
	  }
	}, {
	    freezeTableName: true
	});
	
	return ContactUs;
}