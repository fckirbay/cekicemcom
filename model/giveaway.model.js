module.exports = (sequelize, Sequelize) => {
	const Giveaway = sequelize.define('giveaway', {
	  id: {
          type: Sequelize.STRING,
          primaryKey: true,
          autoIncrement: true
	  },
	  code: {
		type: Sequelize.STRING
	  },
	  user_id: {
		  type: Sequelize.STRING
	  },
	  shortcode: {
		  type: Sequelize.STRING
	  },
	  shortcode_2: {
		type: Sequelize.STRING
	},
	shortcode_3: {
		type: Sequelize.STRING
	},
	shortcode_active: {
		type: Sequelize.STRING
	},
	  display_url: {
		  type: Sequelize.STRING
	  },
	  comment_count: {
		  type: Sequelize.STRING
	  },
	  comment_count_2: {
		type: Sequelize.STRING
	},
	comment_count_3: {
		type: Sequelize.STRING
	},
	  downloaded_comment_count: {
		type: Sequelize.STRING
	  },
	  request_count: {
		type: Sequelize.STRING
	  },
	  like_count: {
		  type: Sequelize.STRING
	  },
	  owner_id: {
		  type: Sequelize.STRING
	  },
	  is_verified: {
		  type: Sequelize.STRING
	  },
	  profile_pic_url: {
		  type: Sequelize.STRING
	  },
	  username: {
		  type: Sequelize.STRING
	  },
	  full_name: {
		  type: Sequelize.STRING
	  },
	  media_count: {
		  type: Sequelize.STRING
	  },
	  follower_count: {
		  type: Sequelize.STRING
	  },
	  is_ad: {
		  type: Sequelize.STRING
	  },
	  caption: {
		  type: Sequelize.STRING
	  },
	  post_date: {
		  type: Sequelize.STRING
	  },
	  title: {
		  type: Sequelize.STRING
	  },
	  winner_count: {
		  type: Sequelize.STRING
	  },
	  reserve_winner_count: {
		  type: Sequelize.STRING
	  },
	  participation: {
		  type: Sequelize.STRING
	  },
	  tag_count: {
		  type: Sequelize.STRING
	  },
	  must_followed: {
		  type: Sequelize.STRING
	  },
	  package_id: {
		  type: Sequelize.STRING
	  },
	  status: {
		type: Sequelize.STRING
	  },
	  created_at: {
		  type: Sequelize.DATE
	  },
	  updated_at: {
		  type: Sequelize.DATE
	  }
	}, {
	    freezeTableName: true
	}, {
	    timestamps: false
	});
	
	return Giveaway;
}