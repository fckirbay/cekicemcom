module.exports = (sequelize, Sequelize) => {
	const Winners = sequelize.define('winners', {
	  id: {
          type: Sequelize.STRING,
          primaryKey: true,
          autoIncrement: true
	  },
	  giveaway_id: {
		  type: Sequelize.STRING
	  },
	  username: {
		  type: Sequelize.STRING
	  },
	  full_name: {
		type: Sequelize.STRING
	  },
	  profile_pic_url: {
		  type: Sequelize.STRING
	  },
	  type: {
		  type: Sequelize.STRING
	  },
	  order: {
		  type: Sequelize.STRING
	  },
	  status: {
		type: Sequelize.STRING
	  },
	  comment_text: {
		type: Sequelize.STRING
	  },
	  created_at: {
		  type: Sequelize.DATE
	  },
	  follow_control: {
		type: Sequelize.STRING
	  },
	  owner_id: {
		type: Sequelize.STRING
	  }
	}, {
	    freezeTableName: true
	}, {
	    timestamps: false
	}, {
		charset: 'utf8mb4',
		collate: 'utf8mb4_general_ci'
	});
	
	return Winners;
}