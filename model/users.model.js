module.exports = (sequelize, Sequelize) => {
	const Users = sequelize.define('users', {
		id: {
			type: Sequelize.STRING,
			primaryKey: true,
			autoIncrement: true
		},
		name_surname: {
			type: Sequelize.STRING
		},
		email: {
			type: Sequelize.STRING
		},
		password: {
			type: Sequelize.STRING
		},
		phone_number: {
			type: Sequelize.STRING
		},
		lang: {
			type: Sequelize.STRING
		},
		verification: {
			type: Sequelize.STRING
		},
		is_admin: {
			type: Sequelize.STRING
		},
		createdAt: {
			type: Sequelize.DATE
		},
		updatedAt: {
			type: Sequelize.DATE
		}
	  }, {
		  freezeTableName: true
	  }, {
		  timestamps: false
	  });
	  
	  return Users;
  }