module.exports = (sequelize, Sequelize) => {
	const Sales = sequelize.define('sales', {
      id: {
		  type: Sequelize.STRING,
		  primaryKey: true,
		  autoIncrement: true
	  },
	  user_id: {
		  type: Sequelize.STRING
	  },
	  package_id: {
		  type: Sequelize.STRING
	  },
	  price: {
		type: Sequelize.STRING
	  },
	  remaining: {
		  type: Sequelize.STRING
	  },
	  trans_id: {
		  type: Sequelize.STRING
	  },
	  payment_type: {
		  type: Sequelize.STRING
	  },
	  is_complete: {
		  type: Sequelize.STRING
	  },
	  createdAt: {
		  type: Sequelize.DATE
	  },
	  updatedAt: {
		  type: Sequelize.DATE
	  },
	  trx_code: {
		  type: Sequelize.STRING
	  },
	  error_code: {
		  type: Sequelize.STRING
	  },
	  message: {
		  type: Sequelize.STRING
	  },
	  result_message: {
		  type: Sequelize.STRING
	  },
	  result_code: {
		  type: Sequelize.STRING
	  }
	}, {
	    freezeTableName: true
	}, {
	    timestamps: false
	});
		
	return Sales;
}