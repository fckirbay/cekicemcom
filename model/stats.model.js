module.exports = (sequelize, Sequelize) => {
	const Stats = sequelize.define('stats', {
	  user_cnt: {
		  type: Sequelize.STRING
	  },
	  raffle_cnt: {
		  type: Sequelize.STRING
	  },
	  winner_cnt: {
		  type: Sequelize.STRING
	  },
	  comment_cnt: {
		  type: Sequelize.STRING
	  }
	}, {
	    freezeTableName: true
	}, {
	    timestamps: false
	});
	
	return Stats;
}