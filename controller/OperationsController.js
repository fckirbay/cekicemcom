var mysql = require('mysql');
const env = require('../config/env.js');

const InstaApi = require('./instaApiClient.js');

var OperationsController = function(){

};

var getGiveAwayInfo = function(connection,code,cb){
    let sql='select * from giveaway where code=?';
    connection.query(sql, [code], function (err, result) {
        if(err){
            cb(err);
            return;
        }
        //console.log(result);
        cb(null,result?result[0]:{});
    });
}

var getGiveAwayInProgressInfo = function(connection,status,cb){
    let sql='select * from giveaway where status=?';
    connection.query(sql, [status], function (err, result) {
        if(err){
            cb(err);
            return;
        }
        //console.log(result);
        cb(null,result);
    });
}

var updateGiveAwayStatus = function(connection,id,count,status,after,shortcodeActive,cb){
    if(status == 1){
        var sql = "UPDATE giveaway set downloaded_comment_count=ifnull(downloaded_comment_count,0)+?,request_count=request_count+1"
        +",status=?,after=?,shortcode_active=? where id=?";
        //console.log(sql,count,status,after);
        connection.query(sql, [count,status,after,shortcodeActive,id], function (err, result) {
            cb(err,result);
        });
    } else if(status == 3) {
        var sql = "UPDATE giveaway set status = ? where id = ?";
        //console.log(sql,count,status,after);
        connection.query(sql, [status, id], function (err, result) {
            cb(err,result);
        });
    } else {
        var sql = "UPDATE giveaway set downloaded_comment_count=ifnull(downloaded_comment_count,0)+?,request_count=CEILING(comment_count/50)"
                +",status=?,after=? where id=?";
        //console.log(sql,count,status,after);
        connection.query(sql, [count,status,after,id], function (err, result) {
            cb(err,result);
        });
    }
    
}

var userCountInComment = function(text){
    var users = {};
    for (let index = 0; index < text.length; index++) {
        const element = text[index];
        if(element == "@"){
            let nextText = text.substr(index);
            let u = nextText.substr(0,nextText.indexOf(' '));
            users[u]="1";
        }
    }
    return Object.keys(users).length;
}

var saveComments = function(connection,mediaId,response,tagUserCount,cb){
    var count = 0;
    var correctCommentCount=0;
    var edges = [];

    if(response && response.edges && response.edges.length>0){
        response.edges.forEach(function(edge){
            var temp = [];
            temp.push(edge.node.id);
            temp.push(edge.node.created_at);
            temp.push(edge.node.owner.id);
            if(edge.node.owner.is_verified == "true") {
                temp.push(1);
            } else {
                temp.push(0);
            }
            temp.push(edge.node.owner.profile_pic_url);
            temp.push(edge.node.owner.username);
            temp.push(edge.node.owner.full_name);
            if(edge.node.viewer_has_liked == "true") {
                temp.push(1);
            } else {
                temp.push(0);
            }
            //console.log("@ count",userCountInComment(edge.node.text));
            //console.log('comment',edge.node.text
            //.replace(/([\u2700-\u27BF]|[\uE000-\uF8FF]|\uD83C[\uDC00-\uDFFF]|\uD83D[\uDC00-\uDFFF]|[\u2011-\u26FF]|\uD83E[\uDD10-\uDDFF])/g
            //, ''));
            temp.push(edge.node.text
                .replace(/([\u2700-\u27BF]|[\uE000-\uF8FF]|\uD83C[\uDC00-\uDFFF]|\uD83D[\uDC00-\uDFFF]|[\u2011-\u26FF]|\uD83E[\uDD10-\uDDFF])/g
                , '')); 
            let calcTagUserCount = userCountInComment(edge.node.text);
            //console.log('tagUserCount',tagUserCount,calcTagUserCount);
            if(calcTagUserCount >= tagUserCount){
                edges.push(temp);
                correctCommentCount++;
            }
            count++; 
            if(count == response.edges.length) {
                console.log(count,response.edges.length,'correctCommentCount',correctCommentCount);
                if(correctCommentCount==0){
                    cb(null,correctCommentCount);
                    return;
                }  
                var sql = "INSERT INTO ck_comments_"+mediaId+" (id, created_at, owner_id, is_verified, profile_pic_url, username, full_name,"
                            +"viewer_has_liked,comment_text) VALUES ?";
                //console.log(sql,edges);
                connection.query(sql, [edges], function (err, result) {
                if (err) {
                    console.log(err);
                    cb(err,correctCommentCount);
                    return;
                }           
                cb(null,correctCommentCount);
                });
            }
        });
    }else{
        cb(null,correctCommentCount);
    }

    
}

var getCommentsWithClient = function(client,shortCode, first, after,cb){
    let getCommentError = null;
    client.getMediaComments({ shortcode: shortCode, first: first, after: after })
    .catch((error) => {
        //console.log('client.getMediaComments Error',error);
        getCommentError=error;
        cb(error);
        return;
    })
    .then((commentResponse) => {
        if(getCommentError){
            return;
        }
        //console.log('client.getMediaComments then',commentResponse);
        if(!commentResponse){
            cb('commentResponseNull');
            return;
        }
        //let commentResponse = body.data.shortcode_media.edge_media_to_parent_comment;
        let has_next_page= commentResponse.page_info.has_next_page;
        let end_cursor= commentResponse.page_info.end_cursor; 
        cb(null,commentResponse,has_next_page,end_cursor,client);
        /*saveCommentInDb(connection,mediaId,commentResponse,function(err){}); 
        if(has_next_page){
            let end_cursor= commentResponse.page_info.end_cursor; 
            latestAfter = end_cursor;
            let commentCount= commentResponse.count; 
            latestCommentCount = commentCount;
            cb(null, commentCount,end_cursor);
        } 
        else {
            cb('has_next_page null');
        }*/
    });
}


var fetchComments = function(id,shortCode,shortCode2,shortCode3,shortcodeActive,tagUserCount,gvAfter,cb){
    var connection = mysql.createConnection({
        host: env.host,
        user: env.username,
        password: env.password,
        database: env.database,
        charset : 'utf8mb4'
    });
    connection.connect(function(err) {
        if(err){
            console.log(err);
        } 
        //var client = new Instagram({ username, password});
        var commentErrCount = 0;
        var fetchingStatus = 1;
        var completedStatus = 2;
        var giveAwayStatus = fetchingStatus;
        var nextPost = false;

        var first = 50;
        var after = gvAfter;
        var recursiveCb = function(err,comments,hasNextPage,endCursor,client){
            console.log('recursiveCb',hasNextPage,endCursor);
            if(err){
                console.log(recursiveCb,err.name,err.message,err.error); 
            }
            if(err){
                commentErrCount++;
                InstaApi.proxyAndLogin(
                    commentErrCount,
                    function(client){
                        //commentErrCount=0;
                        getCommentsWithClient(
                            client,
                            shortcodeActive, 
                            first, 
                            after,
                            recursiveCb);
                    },err);
            }else{

                after = endCursor;

                if(nextPost == true) {
                    nextPost = false;
                }

                if(!hasNextPage){
                    //console.log(null,'ended');
                    console.log("birinci post bitti")
                    if(shortcodeActive == shortCode && shortCode2 != null) {
                        console.log("ikinci post başladı")
                        shortcodeActive = shortCode2;
                        after = null;
                        nextPost = true;
                    } else if(shortcodeActive == shortCode2 && shortCode3 != null) {
                        shortcodeActive = shortCode3;
                        after = null;
                        nextPost = true;
                    } else {
                        console.log("çekimler bitti")
                        giveAwayStatus = completedStatus;
                    }
                }

                
                //console.log('saveComments before',after,hasNextPage);
                saveComments(connection,
                    id,
                    comments,
                    tagUserCount,
                    function(err,commentCount){
                        //console.log('saveComments cb',err,commentCount,hasNextPage);
                        
                        updateGiveAwayStatus(connection,
                            id,
                            commentCount,
                            giveAwayStatus,
                            after,
                            shortcodeActive,
                            function(err,result){
                            if(!hasNextPage && !nextPost){
                                cb(null,'ended');
                                console.log('connection.end');
                                connection.end();
                            }
                        });
                });
                if(hasNextPage || nextPost == true){
                    getCommentsWithClient(
                        client,
                        shortcodeActive, 
                        first, 
                        after,
                        recursiveCb);
                }else{
                    //connection.end();
                    console.log(null,'ended');
                }
            }
        }; 

        InstaApi.proxyAndLogin(0,
        function(client){
            getCommentsWithClient(client,shortCode, first, after,recursiveCb);
        });
    });
};

var createTables = function(connection,id,cb){
    var tableDef = 'CREATE TABLE ck_comments_'+id
    +' (c_id int(11) NOT NULL AUTO_INCREMENT,id varchar(250),created_at varchar(15)'
    +',owner_id varchar(20),full_name VARCHAR(100) null ,is_verified varchar(10), profile_pic_url varchar(1024),'
    +' username varchar(1024), viewer_has_liked varchar(10)'
    +',comment_text varchar(4000)'
    +',PRIMARY KEY (c_id)) CHARSET=utf8mb4;';
    connection.query(tableDef,function (err, results, fields) {
        //console.log(err);
        if(err){
            cb (err);
            return;
        }
        cb(null,'ok');
    }); 
};

var getRandomIntInclusive = function(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1) + min); 
    //The maximum is inclusive and the minimum is inclusive
}

var getWinner = function(connection,id,type,commentCount,cb){
    let winnerCommentId = getRandomIntInclusive(1,commentCount);
    let sql = "select * from ck_comments_"+id+" where c_id=?";

    connection.query(sql,[winnerCommentId],function(err,result){
        let winner = null;
        if(result && result[0]){
            winner = {
                comment_text: result[0].comment_text,
                order_no: null,
                owner_id: result[0].owner_id,
                username : result[0].username,
                profile_pic_url : result[0].profile_pic_url,
                full_name : result[0].full_name,
                giveaway_id : id,
                type : type,
                status: 0
            };
        }
        cb(err,winner);
    });
}

var getWinners = function(distinctWinners,connection,id,type,commentCount,winnerCount,cb){
    var cbCounter = 0;
    var winnerList = []; 
    var error = null;
    var inCb = function(err,winner){
        //console.log(distinctWinners,winner.username);
        error = err;
        if(!distinctWinners[winner.username]){
            distinctWinners[winner.username]=winner;
            cbCounter++;
            winner.order_no = cbCounter;
            winnerList.push(winner);
        }
        if(winnerCount==cbCounter){
            cb(error,winnerList);
        }else{
            getWinner(connection,id,type,commentCount,inCb);
        }
    };
    //for (let index = 1; index <= winnerCount; index++) {
    getWinner(connection,id,type,commentCount,inCb);
    //}
}

var saveWinners = function(connection,id,winnerList,mustFollowed,cb){
    let sql = 'insert into winners(giveaway_id,username,profile_pic_url,type,order_no,full_name,comment_text,follow_control,owner_id) values ?';
    var insertlist = [];

    var followList = {};
    
    if(mustFollowed != null) {
        var list  = mustFollowed.split(',');
        list.forEach(follow => {
            followList[follow] = 0;
        });
    }
    for (let index = 0; index < winnerList.length; index++) {
        const element = winnerList[index];
        let win = [];
        win.push(element.giveaway_id);
        win.push(element.username);
        win.push(element.profile_pic_url);
        win.push(element.type);
        win.push(element.order_no);
        win.push(element.full_name);
        win.push(element.comment_text);
        win.push(JSON.stringify(followList));
        win.push(element.owner_id);
        insertlist.push(win);
    }
    connection.query(sql,[insertlist],function(err,result){
        cb(err,result);
    });
};

var deleteWinners = function(connection,id,cb){
    let sql = 'delete from winners where giveaway_id=?';
    connection.query(sql,[id],function(err,result){
        cb(err,result);
    });
};

var runGiveAway = function(connection,id,commentCount,winnerCount,reserveWinnerCount,mustFollowed,cb){
    var totalWinnerList = [];
    var totalDistintWinners = {};
    getWinners(totalDistintWinners,
        connection,
        id,1,
        commentCount,
        winnerCount,
        function(err,winnerList){
        if(err){
            cb(err);
            return;
        }
        for (let index = 0; index < winnerList.length; index++) {
            const element = winnerList[index];
            totalWinnerList.push(element);
        }
        getWinners(totalDistintWinners,
            connection,
            id,2,
            commentCount,
            reserveWinnerCount,
            function(err,winnerList){
            if(err){
                cb(err);
                return;
            }
            for (let index = 0; index < winnerList.length; index++) {
                const element = winnerList[index];
                totalWinnerList.push(element);
            }
            deleteWinners(connection,id,function(err,result){
                if(err){
                    cb(err);
                    return;
                }
                saveWinners(connection,id,totalWinnerList,mustFollowed,function(err,result){
                    cb(err,totalWinnerList);
                });
            });   
        });
    });
};

var getMediaInfo = function(client, mediaShortCode, cb){
    var mediaInfos = {};
    console.log("mediaShortCode", mediaShortCode);
    client.getMediaByShortcode({ shortcode: mediaShortCode[0] }).then((data)=>{
        if(!data.graphql && !data.graphql.shortcode_media) {
            cb("error", null);
        } else {
            const media = data.graphql.shortcode_media;
            mediaInfos.shortcode = media.shortcode;
            mediaInfos.display_url = media.display_url;
            mediaInfos.comment_count = media.edge_media_to_parent_comment.count;
            mediaInfos.comment_count_2 = 0;
            mediaInfos.comment_count_3 = 0;
            mediaInfos.like_count = media.edge_media_preview_like.count;
            mediaInfos.owner_id = media.owner.id;
            mediaInfos.owner_is_verified = media.owner.is_verified;
            mediaInfos.owner_profile_pic_url = media.owner.profile_pic_url;
            mediaInfos.owner_username = media.owner.username;
            mediaInfos.owner_full_name = media.owner.full_name;
            mediaInfos.owner_timeline_media_count = media.owner.edge_owner_to_timeline_media.count;
            mediaInfos.owner_follower_count = media.owner.edge_followed_by.count;
            mediaInfos.is_ad = media.is_ad;
            if(media.edge_media_to_caption && media.edge_media_to_caption.edges && media.edge_media_to_caption.edges.length > 0) {
                mediaInfos.caption = media.edge_media_to_caption.edges[0].node.text;
            } else {
                mediaInfos.caption = '';
            }
            mediaInfos.datetime = media.taken_at_timestamp;
            if(mediaShortCode[1]) {
                client.getMediaByShortcode({ shortcode: mediaShortCode[1] }).then((data)=>{
                    if(!data.graphql && !data.graphql.shortcode_media) {
                        cb("error", null);
                    } else {
                        const media = data.graphql.shortcode_media;
                        mediaInfos.shortcode_2 = media.shortcode;
                        mediaInfos.comment_count_2 = media.edge_media_to_parent_comment.count;
                        if(mediaShortCode[2]) {
                            client.getMediaByShortcode({ shortcode: mediaShortCode[2] }).then((data)=>{
                                if(!data.graphql && !data.graphql.shortcode_media) {
                                    cb("error", null);
                                } else {
                                    const media = data.graphql.shortcode_media;
                                    mediaInfos.shortcode_3 = media.shortcode;
                                    mediaInfos.comment_count_3 = media.edge_media_to_parent_comment.count;
                                    cb(null, mediaInfos);
                                }
                            });
                        } else {
                            cb(null, mediaInfos);
                        }
                    }
                });
            } else {
                cb(null, mediaInfos);
            }
            
        }
    })
};

OperationsController.prototype.getMediaInfo = function(req,res,next) {

    var shortcodes = [];
    
    var mediaUrl = req.body.url;
    var mediaShortCodeSplits = mediaUrl.split("/");
    var mediaShortCode = mediaShortCodeSplits[4];
    shortcodes.push(mediaShortCode);

    if(req.body.url2 != null && req.body.url2 != "") {
        var mediaUrl2 = req.body.url2;
        var mediaShortCodeSplits2 = mediaUrl2.split("/");
        var mediaShortCode2 = mediaShortCodeSplits2[4];
        shortcodes.push(mediaShortCode2);
    }

    if(req.body.url3 != null && req.body.url3 != "") {
        var mediaUrl3 = req.body.url3;
        var mediaShortCodeSplits3 = mediaUrl3.split("/");
        var mediaShortCode3 = mediaShortCodeSplits3[4];
        shortcodes.push(mediaShortCode3);
    }

    var errCount = 0;

    var recursiveCb = function(err, mediaInfos) {
        if(err) {
            errCount++;
            InstaApi.proxyAndLogin(errCount,
                function(client){
                    getMediaInfo(client, shortcodes, recursiveCb);
                }
            );
        } else {
            res.status(200).json({
                "data": mediaInfos
            });
        }
    }

    InstaApi.proxyAndLogin(errCount,
        function(client) {
            getMediaInfo(client, shortcodes, recursiveCb);
        }
    );

};

OperationsController.prototype.fetchComments = function(req,res,next){   
    var code = req.query.code; 
    var tagUserCount = 0; 
    var connection = res.locals.connection;
    getGiveAwayInfo(connection,code,function(err,giveAway){
        if(err){
            console.log(err);
            next(err);
            return;
        }
        tagUserCount = giveAway.tag_count?giveAway.tag_count:0;
        if(giveAway.status && giveAway.status==0){
            updateGiveAwayStatus(connection,
                giveAway.id,0,1,"",giveAway.shortcode,
                function(err,result){});

            createTables(connection,giveAway.id,function(err,result){
                //error not important
                res.locals.response = {
                    code: 0,
                    message : "started"
                }
                fetchComments(giveAway.id,
                    giveAway.shortcode,
                    giveAway.shortcode_2,
                    giveAway.shortcode_3,
                    giveAway.shortcode_active,
                    tagUserCount,
                    "",
                    function(err,result){
                });
                next();
            });
        }else{
            res.locals.response = {
                code: 0,
                message : "already started"
            }
            next();
        }
    });
};

OperationsController.prototype.doGiveAway = function(req,res,next){   
    var code = req.query.code; 
    var connection = res.locals.connection;
    getGiveAwayInfo(connection,code,function(err,giveAway){
        if(err){
            console.log(err);
            next(err);
            return;
        }
        runGiveAway(connection,giveAway.id,
            giveAway.downloaded_comment_count,
            giveAway.winner_count,
            giveAway.reserve_winner_count,
            giveAway.must_followed,
            function(err,winners){
            if(err){
                next(err);
                return;
            }
            updateGiveAwayStatus(connection,
                giveAway.id,0,3,"",giveAway.shortcode,
                function(err,result){
                    res.locals.response = {
                        code: 0,
                        message : "ok",
                        data : {
                            winners : winners
                        }
                    }
                    next();
            });
            
        });
    });
};

var randomComments = function(connection, id, cb){
    let sql='select username, full_name, profile_pic_url from ck_comments_'+id+' order by id desc limit 100';
    connection.query(sql, [], function (err, result) {
        if(err){
            cb(err);
            return;
        }
        //console.log(result);
        cb(null,result);
    });
}

OperationsController.prototype.getRandomComments = function(req,res,next){   
    var code = req.query.code; 
    var connection = res.locals.connection;
    getGiveAwayInfo(connection, code, function(err, giveAway){
        if(err){
            console.log(err);
            next(err);
            return;
        }
        randomComments(connection, giveAway.id, function(err, comments){
            if(err){
                next(err);
                return;
            }
            res.locals.response = {
                code: 0,
                message : "ok",
                data : {
                    comments : comments
                }
            }
            next();
        });
    });
};


var setWinnerStatus = function(connection, id, order_no, status, type, cb){
    let sql='update winners set status = ? where giveaway_id = ? and order_no = ? and type = ?';
    connection.query(sql, [status, id, order_no, type], function (err, result) {
        if(err){
            cb(err);
            return;
        }
        //console.log(result);
        cb(null,result);
    });
}

OperationsController.prototype.setWinnerStatus = function(req,res,next){   
    var code = req.body.code;
    var order_no = req.body.order_no;
    var status = req.body.status;
    var type = req.body.type;
    var connection = res.locals.connection;
    
    getGiveAwayInfo(connection, code, function(err, giveAway){
        if(err){
            console.log(err);
            next(err);
            return;
        }
        if(giveAway) {
            setWinnerStatus(connection, giveAway.id, order_no, status, type, function(err, setStatus){
                if(err){
                    next(err);
                    return;
                }
                res.locals.response = {
                    code: 0,
                    message : "ok",
                    data : {
                        status : true
                    }
                }
                next();
            });
        } else {
            res.locals.response = {
                code: 0,
                message : "ok",
                data : {
                    status : false
                }
            }
            next();
        }
        
    });
};

var testComments=function(){
    var code = "LA06U3";
    var tagUserCount = 0;
    connection = mysql.createConnection({
        host: env.host,
        user: env.username,
        password: env.password,
        database: env.database,
        charset : 'utf8mb4'
    });
    connection.connect(function(err) {
        if(err){
            console.log(err);
            return;
        } 
        getGiveAwayInfo(connection,
            code,
            function(err,giveAway){
            if(err){
                console.log(err);
                return;
            }
            createTables(connection,
                giveAway.id,
                function(err,result){
                //error not important
                fetchComments(giveAway.id,
                    giveAway.shortcode,
                    tagUserCount,
                    "",
                    function(err,result){
                        console.log('fetchComments',err,result);
                });
            });
        });
    });
};
 
 var testGiveAway = function(){
    var code = "LA06U3";
    //var mysql = require('mysql'); 
    connection = mysql.createConnection({
        host: env.host,
        user: env.username,
        password: env.password,
        database: env.database,
        charset : 'utf8mb4'
    });
    connection.connect(function(err) {
        if(err){
            console.log(err);
            return;
        } 
        getGiveAwayInfo(connection,code,function(err,giveAway){
            if(err){
                console.log(err);
                return;
            }
            runGiveAway(connection,giveAway.id,
                giveAway.downloaded_comment_count,
                giveAway.winner_count,
                giveAway.reserve_winner_count,
                function(err,result){
                    console.log(err,'runGiveAway complete')
            });
        });
    });
}

//testGiveAway();
//testComments();


var startProgressComments = function(){
    var fetchingStatus = 1;
    //var tagUserCount = 0;
    connection = mysql.createConnection({
        host: env.host,
        user: env.username,
        password: env.password,
        database: env.database,
        charset : 'utf8mb4'
    });
    connection.connect(function(err) {
        if(err){
            console.log(err);
            return;
        } 
        getGiveAwayInProgressInfo(connection,
            fetchingStatus,
            function(err,giveAwayList){
            if(err){
                console.log(err);
                connection.end();
                return;
            }

            var processGiveAway = function(giveAway,cb){
                createTables(connection,
                    giveAway.id,
                    function(err,result){
                    //error not important
                    fetchComments(giveAway.id,
                        giveAway.shortcode,
                        giveAway.shortcode_2,
                        giveAway.shortcode_3,
                        giveAway.shortcode_active,
                        giveAway.tag_count,
                        giveAway.after,
                        cb);
                });
            }

            if(giveAwayList && giveAwayList.length>0){
                var giveAwayListLength=giveAwayList.length;
                var cbCounter = 0;

                var totalCallBack = function(err,result){
                    cbCounter++;
                    if(giveAwayListLength==cbCounter){
                        console.log('total complete');
                        connection.end();
                        return;
                    }else{
                        processGiveAway(giveAwayList[cbCounter],totalCallBack);
                    }
                }
                processGiveAway(giveAwayList[cbCounter],totalCallBack);
            }else{
                console.log('giveAwayList count 0');
                connection.end();
            }            
        });
    });
}

var getWinnerInfo = function(connection, giveaway_id, owner_id, cb){
    let sql='select * from winners where giveaway_id = ? and owner_id = ?';
    connection.query(sql, [giveaway_id, owner_id], function (err, result) {
        if(err){
            cb(err);
            return;
        }
        //console.log(result);
        cb(null,result?result[0]:{});
    });
}

var getUserFollowList = function(client, ownerId, first, after, cb){


    
    client.getFollowings({ userId: ownerId, first: first, after: after })
    .catch((error) => {
        cb(error);
        return;
    })
    .then((followingResponse) => {
        if(!followingResponse){
            cb('followingResponseNull');
            return;
        }
        let has_next_page= followingResponse.page_info.has_next_page;
        let end_cursor= followingResponse.page_info.end_cursor; 
        cb(null,followingResponse,has_next_page,end_cursor,client);
    });
}

var updateWinnerFollowControl = function(connection, giveaway_id, owner_id, followResultJson, cb) {
    let sql='update winners set follow_control = ? where giveaway_id = ? and owner_id = ?';
    connection.query(sql, [JSON.stringify(followResultJson), giveaway_id, owner_id], function (err, result) {
        if(err){
            cb(err);
            return;
        }
        //console.log(result);
        cb(null, true);
    });
}



OperationsController.prototype.setFollowControl = function(req,res,next){   
    var code = req.body.code;
    var owner_id = req.body.owner_id;
    var connection = res.locals.connection;

    var followings = [];

    var first = 50;
    var after = null;

    getGiveAwayInfo(connection, code, function(err, giveAway){
        if(err){
            console.log(err);
            next(err);
            return;
        }
        if(giveAway) {
            getWinnerInfo(connection, giveAway.id, owner_id, function(err, winnerInfo){
                if(err){
                    next(err);
                    return;
                }
                var errCount = 0;
                var followResult = [];
                var followResultJson = {};
                var followeds = giveAway.must_followed.split(',');
                /*followeds.forEach(followed => {
                    followResult.push({ username: followed, status: 0});
                });*/

                var recursiveCb = function(err, followingList, has_next_page, end_cursor, client) {
                    if(err) {
                        errCount++;
                        InstaApi.proxyAndLogin(errCount,
                            function(client){
                                getUserFollowList(client, winnerInfo.owner_id, first, after, recursiveCb);
                            }
                        );
                    } else {
                        followingList.data.forEach(following => {
                            followings.push(following.username);
                        });
                        if(followingList.page_info.has_next_page == true) {
                            getUserFollowList(client, winnerInfo.owner_id, first, followingList.page_info.end_cursor, recursiveCb);
                        } else {
                            followeds.forEach(followed => {
                                var followCount = 0;
                                followings.forEach(following => {
                                    if(followed.trim() == following) {
                                        followCount++;
                                    }
                                });
                                if(followCount > 0) {
                                    followResult.push({ username: followed.trim(), status: 1});
                                    followResultJson[followed.trim()] = 1;
                                } else if(followings.length == 0) {
                                    followResult.push({ username: followed.trim(), status: 3});
                                    followResultJson[followed.trim()] = 3;
                                } else {
                                    followResult.push({ username: followed.trim(), status: 2});
                                    followResultJson[followed.trim()] = 2;
                                }
                            });

                            updateWinnerFollowControl(connection, giveAway.id, owner_id, followResultJson, function(err, updateInfo) {
                                if(err){
                                    next(err);
                                    return;
                                }
                                res.locals.response = {
                                    code: 0,
                                    message : "ok",
                                    data : followResult
                                }
                                next();
                            });
                           
                        }
                    }
                }
            
                InstaApi.proxyAndLogin(errCount,
                    function(client) {
                        getUserFollowList(client, winnerInfo.owner_id, first, after, recursiveCb);
                    }
                );
                
            });
        } else {
            res.locals.response = {
                code: 0,
                message : "ok",
                data : null
            }
            next();
        }
        
    });
};

startProgressComments();

module.exports = OperationsController; 