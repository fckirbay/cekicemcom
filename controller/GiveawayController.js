const db = require("../config/db.config.js");
const config = require("../config/config.js");
const Giveaway = db.giveaway;
const Sales = db.sales;
const Detail = db.detail;

const Op = db.Sequelize.Op;

var nodemailer = require("nodemailer");

var mailTransporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: "devfirattest@gmail.com",
    pass: "Q1w2e3xas",
  },
});

exports.get = (req, res) => {
  req.filters["user_id"] = req.userId;

  if (!req.fields) {
    var attributes = [
      "id",
      "code",
      "user_id",
      "shortcode",
      "shortcode_2",
      "shortcode_3",
      "shortcode_active",
      "display_url",
      "comment_count",
      "comment_count_2",
      "comment_count_3",
      "downloaded_comment_count",
      "request_count",
      "like_count",
      "owner_id",
      "is_verified",
      "profile_pic_url",
      "username",
      "full_name",
      "media_count",
      "follower_count",
      "is_ad",
      "caption",
      "post_date",
      "title",
      "winner_count",
      "reserve_winner_count",
      "participation",
      "tag_count",
      "must_followed",
      "package_id",
      "status",
      "created_at",
      "updated_at",
    ];
  } else {
    var attributes = req.fields;
  }

  Giveaway.findAll({
    where: req.filters,
    attributes: attributes,
    order: [["id", "DESC"]],
  })
    .then((data) => {
      res.status(200).json({
        data: data,
      });
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
      });
    });
};

exports.detail = (req, res) => {
  if (!req.query["code"]) {
    res.status(200).json({
      data: [],
    });
  } else {
    var filters = {};
    filters["code"] = req.query["code"];

    Giveaway.findOne({
      where: filters,
      attributes: [
        "id",
        "code",
        "user_id",
        "shortcode",
        "shortcode_2",
        "shortcode_3",
        "shortcode_active",
        "display_url",
        "comment_count",
        "comment_count_2",
        "comment_count_3",
        "downloaded_comment_count",
        "request_count",
        "like_count",
        "owner_id",
        "is_verified",
        "profile_pic_url",
        "username",
        "full_name",
        "media_count",
        "follower_count",
        "is_ad",
        "caption",
        "post_date",
        "title",
        "winner_count",
        "reserve_winner_count",
        "participation",
        "tag_count",
        "must_followed",
        "package_id",
        "status",
        "created_at",
        "updated_at",
      ],
      order: [["id", "DESC"]],
    })
      .then((data) => {
        res.status(200).json({
          data: data,
        });
      })
      .catch((err) => {
        res.status(500).json({
          error: err,
        });
      });
  }
};

function makeid(length) {
  var result = "";
  var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

exports.post = (req, res) => {

  if(!req.body.giveaway_id) {
    var code = makeid(6);
    Giveaway.create({
      code: code,
      user_id: req.userId,
      shortcode: req.body.mediaInfo.shortcode,
      shortcode_active: req.body.mediaInfo.shortcode,
      shortcode_2: req.body.mediaInfo.shortcode_2,
      shortcode_3: req.body.mediaInfo.shortcode_3,
      display_url: req.body.mediaInfo.display_url,
      comment_count: req.body.mediaInfo.comment_count,
      comment_count_2: req.body.mediaInfo.comment_count_2,
      comment_count_3: req.body.mediaInfo.comment_count_3,
      like_count: req.body.mediaInfo.like_count,
      owner_id: req.body.mediaInfo.owner_id,
      profile_pic_url: req.body.mediaInfo.owner_profile_pic_url,
      username: req.body.mediaInfo.owner_username,
      full_name: req.body.mediaInfo.owner_full_name,
      media_count: req.body.mediaInfo.owner_timeline_media_count,
      follower_count: req.body.mediaInfo.owner_follower_count,
      caption: req.body.mediaInfo.caption,
      post_date: req.body.mediaInfo.datetime,
      title: req.body.title,
      winner_count: req.body.winner_count,
      reserve_winner_count: req.body.reserve_winner_count,
      participation: req.body.participation,
      tag_count: req.body.tag_count,
      must_followed: req.body.must_followed,
      //package_id: req.body.package_id
    })
    .then((ga) => {

      var mailOptions = {
        from: "devfirattest@gmail.com",
        to: "cekicemcom@gmail.com",
        subject: "Yeni Çekiliş Oluşturuldu - Çekicem",
        text:
          "User ID: " +
          req.userId +
          " Shortcode: " +
          req.body.mediaInfo.shortcode +
          " Yorum Sayısı: " +
          req.body.mediaInfo.comment_count,
	  };

      mailTransporter.sendMail(mailOptions, function (error, info) {
        if (error) {
          console.log("error", error);
        } else {
          console.log("Email sent: " + info.response);
		}
		
	  });

      res.status(200).send({ result: true, code: ga.code });
    })
    .catch((err) => {
      res.status(200).send({ result: false, error: "ERROR_OCURRED!" });
    });
  } else {
    Sales.increment(
      { remaining: -1 }, 
      { where: { user_id: req.userId, package_id: req.body.package } 
    }).then((result) => {
      Giveaway.update(
        {
          title: req.body.title, 
          winner_count: req.body.winner_count, 
          reserve_winner_count: req.body.reserve_winner_count, 
          participation: req.body.participation,
          tag_count: req.body.tag_count,
          must_followed: req.body.must_followed,
          package_id: req.body.package,
          comment_count: req.body.comment_count,
          comment_count_2: req.body.comment_count_2,
          comment_count_3: req.body.comment_count_3,
          like_count: req.body.like_count,
        },
        { where: { id: req.body.giveaway_id } }
        ).then((ga) => {
          res.status(200).send({ result: true, code: ga.code });
        }).catch((err) => {
          res.status(200).send({ result: false, error: "ERROR_OCURRED!" });
        })
    }).catch(err =>
        res.status(500).json({
        "error": err
      })
    )
  }
    
};


exports.put = (req, res) => {

  Giveaway.update(
    { comment_count: req.body.comment_count, like_count: req.body.like_count },
    { where: { id: req.body.id } }
    ).then(result =>
      res.status(200).json({
        "status": 200,
        "result": true
      })
    ).catch(err =>
      res.status(200).json({
        "status": 200,
        "result": false
      })
    )
};