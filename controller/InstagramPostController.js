const db = require('../config/db.config.js');
const config = require('../config/config.js');
const InstaApi = require('./instaApiClient.js');
const Instagram = require('./instagramapi.js');

const Op = db.Sequelize.Op;
const username = config.mainUsername;
const password = config.mainPassword;
//const client = new Instagram({ username, password })

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var client = new Instagram({  });

exports.get = async (req, res) => {

    var mediaInfos = {};
    var mediaUrl = req.body.url;
    var mediaShortCodeSplits = mediaUrl.split("/");
    var mediaShortCode = mediaShortCodeSplits[4];

    InstaApi.proxyAndLogin(0,function(client){
        client.getMediaByShortcode({ shortcode: mediaShortCode }).then((data)=>{
            const media = data.graphql.shortcode_media;
            mediaInfos.shortcode = media.shortcode;
            mediaInfos.display_url = media.display_url;
            mediaInfos.comment_count = media.edge_media_to_parent_comment.count;
            mediaInfos.like_count = media.edge_media_preview_like.count;
            mediaInfos.owner_id = media.owner.id;
            mediaInfos.owner_is_verified = media.owner.is_verified;
            mediaInfos.owner_profile_pic_url = media.owner.profile_pic_url;
            mediaInfos.owner_username = media.owner.username;
            mediaInfos.owner_full_name = media.owner.full_name;
            mediaInfos.owner_timeline_media_count = media.owner.edge_owner_to_timeline_media.count;
            mediaInfos.owner_follower_count = media.owner.edge_followed_by.count;
            mediaInfos.is_ad = media.is_ad;
            if(media.edge_media_to_caption && media.edge_media_to_caption.edges && media.edge_media_to_caption.edges.length > 0) {
                mediaInfos.caption = media.edge_media_to_caption.edges[0].node.text;
            } else {
                mediaInfos.caption = '';
            }
            mediaInfos.datetime = media.taken_at_timestamp;

            res.status(200).json({
                "data": mediaInfos
            });
        })
    }); 
}