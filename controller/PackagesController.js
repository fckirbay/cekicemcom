const db = require('../config/db.config.js');
const config = require('../config/config.js');
const Packages = db.packages;

const Op = db.Sequelize.Op;

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

exports.get = (req, res) => {

	Packages.findAll({
		where: req.filters,
		attributes: ['id', 'name', 'price', 'description', 'post_limit']
	}).then(data => {
		res.status(200).json({
			"data": data
		});
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	})
}