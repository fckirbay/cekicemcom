const db = require('../config/db.config.js');
const config = require('../config/config.js');
const Users = db.users;

const Op = db.Sequelize.Op;

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

exports.get = (req, res) => {

	req.filters['id'] = req.userId;
	
	if(!req.fields) {
		var attributes = ['id', 'name_surname', 'email', 'phone_number', 'password', 'lang', 'verification', 'is_admin', 'createdAt', 'updatedAt']
	} else {
		var attributes = req.fields;
	}

	Users.findOne({
		where: req.filters,
		attributes: attributes
	}).then(data => {
		res.status(200).json({
			"data": data
		});
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	})
}

exports.post = (req, res) => {

	Users.update(
	  { lang: req.body.name_surname, phone_number: req.body.phone_number, lang: req.body.lang },
	  { where: { id: req.userId } }
	)
	  .then(result =>
	    res.status(200).json({
			"data": result
		})
	  )
	  .catch(err =>
	    res.status(500).json({
	    	"status": 500,
			"error": err
		})
	  )
}

exports.updateos = (req, res) => {

	Users.update(
	  { os: req.body.os },
	  { where: { id: req.userId } }
	)
	  .then(result =>
	    res.status(200).json({
			"status": 200
		})
	  )
	  .catch(err =>
	    res.status(500).json({
	    	"status": 500,
			"error": err
		})
	  )
}

exports.updatedevice = (req, res) => {

	Users.update(
	  { firebase: req.body.firebase, os: req.body.os },
	  { where: { id: req.userId } }
	)
	  .then(result =>
	    res.status(200).json({
			"status": 200
		})
	  )
	  .catch(err =>
	    res.status(500).json({
	    	"status": 500,
			"error": err
		})
	  )
}