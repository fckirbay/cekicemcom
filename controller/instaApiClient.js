const config = require('../config/config.js');
const Instagram = require('./instagramapi.js');


var proxyList =
  [
    //'http://51.158.123.35:9999'
    //'http://51.158.123.35:9999',
    //"http://proxybrowsing.com/"
    // "http://muchproxy.com/"
    'http://wzqfyosq-rotate:ov488a4reotf@p.webshare.io:80'
    //'http://161.202.226.194:80'
    //'http://172.104.58.246:8899', 
    //'http://79.110.52.243:3128' 
 ];


const FileCookieStore = require('tough-cookie-filestore2')

var fs = require('fs');
var loginRequestTimeOut = 10000;
var apiRequestTimeOut = 7000;

var clientList = [];

for(var i = 0; i < config.userList.length; i++) {
    const cookieStore = new FileCookieStore(__dirname+'/cookies_'+config.userList[i].mainUsername+'.json');
    var client = new Instagram({ username: config.userList[i].mainUsername, password:config.userList[i].mainPassword ,cookieStore });
    clientList.push({client: client, username: config.userList[i].mainUsername});
}

var getInstaClient = function(proxy,commentErrCount,cb){
    try {
        var selectedClient = clientList[commentErrCount%clientList.length];
        var client = selectedClient.client;
        fs.readFile(__dirname+'/cookies_'+selectedClient.username+'.json', 'utf8', function(err, data) {
            /*if(err){
                cb(err);
                return;
            }*/
            
            if(data.indexOf('ds_user_id')>=0){
                console.log('already login');
                cb(null,client);
                return;
            }else{
                if(proxy){
                    //client.setProxy({pProxy:proxy});
                }
                console.log('client.login()');
                client.setRequestTimeOut({pTimeOut:loginRequestTimeOut});
                client.login().then(function(){
                    console.log('login success');
                    //testComments();
                    cb(null,client);
                    return;
                }).catch(function(cerror){
                    console.log('client.login().catch',cerror);
                });
            }
        }); 
    } catch(e) {
        //cb(e);
        console.log('Error:', e.stack);
        if(proxy){
            //client.setProxy({pProxy:proxy});
        }
        client.setRequestTimeOut({pTimeOut:loginRequestTimeOut});
        client.login().then(function(){
            console.log('login success');
            //testComments();
            cb(null,client);
            return;
        });
    }
};

var proxyAndLogin = function(commentErrCount,cb,err){
    var proxy = null;
    var proxyIndex=0;
    if(commentErrCount==0 && proxyList.length>0){
        proxy = proxyList[proxyIndex];
    }
    if(proxyList.length>0 && commentErrCount>0){ 
        proxyIndex = commentErrCount % proxyList.length;
        //proxyIndex --;
        var definedProxy = proxyList[proxyIndex];
        if(definedProxy && definedProxy!=' '){
            proxy = definedProxy;
        }
    } 
    var sleepFlag = false;
    if(proxyList.length<4 && commentErrCount > proxyList.length){
        sleepFlag=true;
        for (let index = 0; index < proxyList.length; index++) {
            const element = proxyList[index];
            if(element && element.indexOf("webshare.io")>=0){
                sleepFlag=false;
            }    
        } 
    }
    
    console.log('proxyAndLogin',proxy,"commentErrCount",commentErrCount,"proxyIndex",proxyIndex);
    getInstaClient(proxy,commentErrCount,function(error,client){
        client.setProxy({pProxy:proxy});
        client.setRequestTimeOut({pTimeOut:apiRequestTimeOut});
        if(sleepFlag){
            console.log('setTimeout sleep')
            setTimeout(function () {
                console.log("client.login start",new Date());
                cb(client);
            },4*60*1000);
        }else{
            cb(client);
        }
    });
};

/*proxyAndLogin(0,function(err,client){
    console.log('proxyAndLogin',err);
});*/

module.exports.getInstaClient = getInstaClient;
module.exports.proxyAndLogin = proxyAndLogin;
