const db = require('../../config/db.config.js');
const config = require('../../config/config.js');
var Sequelize = require("sequelize");

const Sales = db.sales;
const Users = db.users;

exports.get = (req, res) => {

	//req.filters['id'] = req.userId;
	
	if(!req.fields) {
		var attributes = ['id', 'user_id', 'package_id', 'price', 'remaining', 'trans_id', 'payment_type', 'is_complete', 'createdAt', 'updatedAt', 'user.name_surname']
	} else {
		var attributes = req.fields;
	}

	Sales.findAll({
		include: [{
			model: Users,
			as: 'user',
			attributes: ['name_surname'],
			required:false
		  }],
		where: req.filters,
		attributes: attributes,
		order: [["id", "DESC"]]
	}).then(data => {
		res.status(200).json({
			"data": data
		});
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	})
}