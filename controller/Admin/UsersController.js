const db = require('../../config/db.config.js');
const config = require('../../config/config.js');
const Users = db.users;

const Op = db.Sequelize.Op;

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

exports.get = (req, res) => {

	//req.filters['id'] = req.userId;
	
	if(!req.fields) {
		var attributes = ['id', 'name_surname', 'email', 'phone_number', 'lang', 'verification', 'is_admin', 'createdAt', 'updatedAt']
	} else {
		var attributes = req.fields;
	}

	Users.findAll({
		where: req.filters,
		attributes: attributes,
		order: [["id", "DESC"]]
	}).then(data => {
		res.status(200).json({
			"data": data
		});
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	})
}