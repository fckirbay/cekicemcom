const db = require('../config/db.config.js');
const config = require('../config/config.js');
var Sequelize = require("sequelize");

const Sales = db.sales;
const Users = db.users;
const Packages = db.packages;

exports.get = (req, res) => {

	req.filters['user_id'] = req.userId;
	req.filters['is_complete'] = 1;

	if(!req.fields) {
		var attributes = ['id', 'user_id', 'package_id', 'price', 'remaining', 'trans_id', 'payment_type', 'is_complete', 'createdAt', 'updatedAt', 'trx_code', 'error_code', 'message', 'result_message', 'result_code', 'user.name_surname', 'package.name', 'package.post_limit', 'package.price']
	} else {
		var attributes = req.fields;
	}

	Sales.findAll({
		include: [{
			model: Users,
			as: 'user',
			attributes: ['name_surname'],
			required:false
		  }, {
			model: Packages,
			as: 'package',
			attributes: ['name', 'price', 'description', 'post_limit'],
			required:false
		  }],
		where: req.filters,
		attributes: attributes,
		order: [
            ['id', 'DESC'],
        ]
	}).then(data => {
		res.status(200).json({
			"data": data
		});
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	})
}