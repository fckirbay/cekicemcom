const db = require('../config/db.config.js');
const config = require('../config/config.js');
const googleTTS = require('google-tts-api');
const { v4: uuidv4 } = require('uuid');

const Packages = db.packages;
const Users = db.users;
const Sales = db.sales;
const Op = db.Sequelize.Op;

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

exports.post = (req, res) => {
    
    // get audio URL
    /*const url = googleTTS.getAudioUrl(req.body.text, {
        lang: 'tr-TR',
        slow: false,
        host: 'https://translate.google.com',
    });

    res.status(200).json({
        "data": url
    });*/
    googleTTS
        .getAudioBase64(req.body.text, { lang: 'tr', slow: false })
        .then((base64) => {
            res.status(200).json({
                "data": base64
            });
        })
        .catch((error) => {
            res.status(500).json({
                "data": error
            });
        });
}