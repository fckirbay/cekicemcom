const db = require('../config/db.config.js');
const config = require('../config/config.js');
const Winners = db.winners;

const Op = db.Sequelize.Op;

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

exports.get = (req, res) => {

	if(!req.query['giveaway_id']) {
		res.status(500).json({
			"error": "Invalid parameters!"
		});
	}

	var filters = {};
	filters['giveaway_id'] = req.query['giveaway_id'];
	
	Winners.findAll({
		where: filters,
		attributes: ['id', 'giveaway_id', 'username', 'full_name', 'profile_pic_url', 'type', 'order_no', 'status', 'comment_text', 'created_at', 'follow_control', 'owner_id']
	}).then(data => {
		res.status(200).json({
			"data": data
		});
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	})
}