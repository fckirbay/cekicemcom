const db = require('../config/db.config.js');
const config = require('../config/config.js');
const { v4: uuidv4 } = require('uuid');

const Packages = db.packages;
const Users = db.users;
const Sales = db.sales;
const Op = db.Sequelize.Op;

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

exports.get = (req, res) => {

	Sales.findOne({
		where: { trans_id: req.query['trans_id'] },
		attributes: ['id', 'user_id', 'package_id', 'price', 'remaining', 'trans_id', 'payment_type', 'is_complete', 'createdAt', 'updatedAt', 'trx_code', 'error_code', 'message', 'result_message', 'result_code'],
	}).then(sale => {
        res.status(200).json({
            "data": sale
        });
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	})
}

exports.post = (req, res) => {

    const weepay = require('weepay-nodejs');
    const { Currency, Locale, PaymentGroup, PaymentChannel, ProductType } = require("../node_modules/weepay-nodejs/weepay/Constants");

    weepay.configure({
        bayiId: config.clientId,
        apiKey: config.apiKey,
        secretKey: config.secretKey,
        baseUrl: "https://test-api.weepay.co"
    });    

    var userId = req.userId;

    var package = req.body.package;
    var count = req.body.count;

    var countText = "";
    var packageName = "";

    if(count == 1) {
        countText = 'ONE_';
    } else if(count == 3) {
        countText = "THREE_";
    } else if(count == 5) {
        countText = "FIVE_";
    } else if(count == 100) {
        countText = "MONTHLY_";
    }

    packageName = countText+package;
    
    Users.findOne({
		where: { id: userId },
		attributes: ['id', 'name_surname', 'email', 'phone_number']
	}).then(user => {
-		Packages.findOne({
            where: { name: packageName },
            attributes: ['id', 'name', 'price', 'description', 'post_limit']
        }).then(pack => {
            var transId = uuidv4();
            const request = {
                data: {
                    orderId: transId,
                    ipAddress: req.ip,
                    paidPrice: pack.price,
                    currency: Currency.TL,
                    locale: Locale.TR,
                    description: "Instagram çekiliş asistanı paketi",
                    callBackUrl: "https://cekicem.com/api/paymentcb",
                    paymentGroup: PaymentGroup.PRODUCT,
                    paymentChannel: PaymentChannel.WEB
                },
                customer: {
                    customerId: user.id,
                    customerName: user.name_surname.split(" ").shift(),
                    customerSurname: user.name_surname.split(" ").slice(-1).pop(),
                    gsmNumber: user.phone_number,
                    email: user.email,
                    identityNumber: "11111111111",
                    city: "istanbul",
                    country: "turkey"
                },
                billingAddress: {
                    contactName: user.name_surname,
                    address: "Akıncılar Mh., Buca",
                    city: "izmir",
                    country: "turkey",
                    zipCode: "35380"
                },
                shippingAddress: {
                    contactName: user.name_surname,
                    address: "Akıncılar Mh., Buca",
                    city: "izmir",
                    country: "turkey",
                    zipCode: "35380"
                },
                products: [
                    {
                        name: pack.description,
                        productPrice: pack.price,
                        itemType: ProductType.VIRTUAL,
                        productId: pack.id
                    }
                ]
            };

            weepay.formInitialize(request).then(function (paymentRes) {
                if (paymentRes.status == "success") {
                    Sales.create({
                        user_id: user.id,
                        package_id: pack.id,
                        price: pack.price,
                        remaining: count,
                        trans_id: transId,
                        payment_type: 1,
                        is_complete: 0
                    }).then(resp => {
                        res.status(200).json({
                            "data": paymentRes.CheckoutFormData
                        });
                    }).catch(err => {
                        res.status(500).json({
                            "error": err
                        });
                    })
                } else {
                    res.status(500).json({
                        "error": paymentRes.message
                    });
                }
            });
        }).catch(err => {
            res.status(500).json({
                "error": err
            });
        })
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	})
}

exports.postcb = (req, res) => {

    var result = req.body;
    var isComplete = 0;
    
    Sales.findOne({
		where: {trans_id: result.orderId, is_complete: 0},
		attributes: ['id', 'user_id', 'package_id', 'price', 'remaining', 'trans_id', 'payment_type', 'is_complete', 'createdAt', 'updatedAt', 'trx_code', 'error_code', 'message', 'result_message', 'result_code'],
	}).then(sale => {

        if(result.paymentStatus == 'true') {
            isComplete = 1;
        } else {
            isComplete = 2;
        }

        Sales.update(
            { is_complete: isComplete, trx_code: result.trxCode, error_code: result.errorCode, message: result.message, result_message: result.resultMessage, result_code: result.resultCode },
            { where: { trans_id: result.orderId, is_complete: 0 } }
        ).then(updateRes =>{
            res.redirect('https://cekicem.com/#/payment?trans_id='+result.orderId)
        }).catch(err => {
            res.redirect('https://cekicem.com/#/payment?trans_id='+result.orderId)
        })
	}).catch(err => {
		res.redirect('https://cekicem.com/#/payment?trans_id='+result.orderId)
	})
}