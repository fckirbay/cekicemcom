const db = require('../config/db.config.js');
const config = require('../config/config.js');
const ContactUs = db.contactus;

const Op = db.Sequelize.Op;

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

exports.post = (req, res) => {

    if(req.body.email == null) {
        req.body.email = req.body.email_login;
	}
	
	ContactUs.create({
		email: req.body.email,
		subject: req.body.subject,
		message: req.body.message
	}).then(resp => {
		res.status(200).send({ sent: true });
	}).catch(err => {
        res.status(200).send({ sent: false });
	})
}