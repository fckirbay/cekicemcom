const db = require('../config/db.config.js');
const config = require('../config/config.js');
const Upcomings = db.upcomings;

const Op = db.Sequelize.Op;

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

exports.get = (req, res) => {

	Upcomings.findAll({
		where: req.filters,
		attributes: ['username', 'caption', 'photo_url', 'date', 'follower_cnt', 'comment_cnt', 'is_verified']
	}).then(data => {
		res.status(200).json({
			"data": data
		});
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	})
}