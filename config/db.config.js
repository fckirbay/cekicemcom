const env = require('./env.js');
 
const Sequelize = require('sequelize');
const sequelize = new Sequelize(env.database, env.username, env.password, {
  host: env.host,
  dialect: env.dialect,
  dialectOptions: {
    charset: 'utf8mb4'
  },
  operatorsAliases: false,
  define: {
    timestamps: false,
    charset: 'utf8mb4',
    collate: 'utf8mb4_general_ci'
  },
  pool: {
    max: env.max,
    min: env.pool.min,
    acquire: env.pool.acquire,
    idle: env.pool.idle
  }
});
 
const db = {};
 
db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.stats = require('../model/stats.model.js')(sequelize, Sequelize);
db.upcomings = require('../model/upcomings.model.js')(sequelize, Sequelize);
db.users = require('../model/users.model.js')(sequelize, Sequelize);
db.sales = require('../model/sales.model.js')(sequelize, Sequelize);
db.contactus = require('../model/contactus.model.js')(sequelize, Sequelize);
db.packages = require('../model/packages.model.js')(sequelize, Sequelize);
db.giveaway = require('../model/giveaway.model.js')(sequelize, Sequelize);
db.detail = require('../model/detail.model.js')(sequelize, Sequelize);
db.winners = require('../model/winners.model.js')(sequelize, Sequelize);

db.sales.belongsTo(db.users, {foreignKey: 'user_id'});
db.users.hasMany(db.sales);

db.sales.belongsTo(db.packages, {foreignKey: 'package_id'});
db.packages.hasMany(db.sales);

/*db.comments.belongsTo(db.posts);
db.posts.hasMany(db.comments);
db.posts.belongsTo(db.users);
db.users.hasMany(db.posts);*/

//db.sales.hasOne(db.users, { through: 'users', foreignKey: 'id'});
//db.sales.hasOne(db.users, {foreignKey: 'user_id'});
//db.user.belongsToMany(db.role, { through: 'user_roles', foreignKey: 'userId', otherKey: 'roleId'});

module.exports = db;