if (annyang) {
    // Let's define our first command. First the text we expect, and then the function it should call
    console.log("xxx");
    var commands = {
      'hello': function() {
        console.log("yyy");
      },
      'hi': function() {
        console.log("yyy");
      },
      'merhaba': function() {
        console.log("yyy");
      }
    };

    console.log("commands", commands);
    // Add our commands to annyang
    annyang.addCommands(commands);
  
    // Start listening. You can call this here, or attach this call to an event, button, etc.
    console.log("started");
    annyang.start();
  }