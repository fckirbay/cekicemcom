import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core'

import { MyProfileService } from './my-profile.service';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  providers: [MyProfileService],
  styleUrls: ['./my-profile.component.css']
})
export class MyProfileComponent implements OnInit {

  userInfo: any = [];
  myProfileForm: any;
  currentLang = localStorage.getItem('lang') || 'tr';

  langs = [
    {code: 'tr', name: 'Turkish'},
    {code: 'en', name: 'English'},
    {code: 'de', name: 'German'},
    {code: 'fr', name: 'French'}
  ];

  constructor(private myProfileService: MyProfileService, private formBuilder: FormBuilder, private translate: TranslateService) { }

  ngOnInit(): void {
    this.getMyProfile();
    this.myProfileForm = this.formBuilder.group({
      name_surname: this.userInfo.name_surname,
      email: this.userInfo.email,
      phone_number: this.userInfo.phone_number,
      lang: this.userInfo.lang
    });
  }

  onSubmit(userInfos: any) {    
    // The server will generate the id for this new hero
    this.myProfileService
      .setMyProfile(userInfos)
      .subscribe(response => {
        console.log(response);
        localStorage.setItem("lang", userInfos.lang);
        this.translate.use(userInfos.lang);
      });
    
  }

  getMyProfile(): void {
    this.myProfileService.getMyProfile()
      .subscribe((user: any) => {
        this.userInfo = user.data;
      });
  }

}
