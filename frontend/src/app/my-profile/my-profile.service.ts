import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpErrorHandler, HandleError } from '../http-error-handler.service';

import { APP_CONFIG, AppConfig } from '../app-config.module';

import { MyProfile } from './models/my-profile';
import { Observable } from 'rxjs';



@Injectable()
export class MyProfileService {
  private handleError: HandleError;

  constructor(
    private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig,
    httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('HomeService');
  }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer '+localStorage.getItem('access_token') || ''
    })
  };

  /* GET heroes whose name contains search term */
  getMyProfile(): Observable<MyProfile[]> {
    return this.http.get<MyProfile[]>(this.config.apiEndpoint+'/users', this.httpOptions)
      .pipe(
        catchError(this.handleError<MyProfile[]>('getMyProfile', []))
      );
  }
  
  /** POST: add a new hero to the database */
  setMyProfile(userInfos: any) {
    return this.http.post(this.config.apiEndpoint+'/myprofile', userInfos, this.httpOptions)
      .pipe(
        catchError(this.handleError('setMyProfile', userInfos))
      );
  }
}
