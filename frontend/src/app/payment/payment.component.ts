import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { PaymentService } from './payment.service';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  providers: [PaymentService],
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {
  
  trans_id: string;
  paymentResult: any;

  constructor(private route: ActivatedRoute, private paymentService: PaymentService) {
    this.route.queryParams.subscribe(params => {
      this.trans_id = params['trans_id'];
    });
  }

  ngOnInit(): void {
    if(this.trans_id) {
      this.getPayment(this.trans_id);
    }
  }

  getPayment(trans_id: any): void {
    this.paymentService.getPayment(trans_id)
      .subscribe((resp: any) => {
        this.paymentResult = resp.data;
      });
  }

}
