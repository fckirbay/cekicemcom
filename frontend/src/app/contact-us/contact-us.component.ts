import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import jwt_decode from 'jwt-decode';

import { ContactUsService } from './contact-us.service';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  providers: [ContactUsService],
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {

  isLogged = false;
  userMail: any;
  contactForm:any;
  submitted = false;
  error = false;

  constructor(private contactUsService: ContactUsService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.isLogged = this.loggedIn;
    if(this.isLogged == true) {
      this.userMail = this.getDecodedAccessToken(localStorage.getItem('access_token')).email;
    }
    this.contactForm = this.formBuilder.group({
      email: '',
      email_login: this.userMail,
      subject: '',
      message: ''
    });
  }

  public get loggedIn(): boolean{
    return localStorage.getItem('access_token') !==  null;
  }

  getDecodedAccessToken(token: any): any {
    try{
        return jwt_decode(token);
    }
    catch(Error){
        return null;
    }
  }

  onSubmit(contactInfos: any) {    
    // The server will generate the id for this new hero
    this.contactUsService
      .setContactUs(contactInfos)
      .subscribe(response => {
        if(response.sent == true) {
          this.submitted = true;
        } else {
          this.error = true;
        }
      });
  }

}
