export interface ContactUs {
    id: String;
    email: String;
    subject: String;
    message: String;
    replied: String;
    createdAt: String;
    updatedAt: String;
  }