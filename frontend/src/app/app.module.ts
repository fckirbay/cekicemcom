import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { MomentModule } from 'ngx-moment';

import { AppConfigModule } from './app-config.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AgGridModule } from 'ag-grid-angular';

import { HomeComponent } from './home/home.component';
import { NavigationComponent } from './shared/navigation/navigation.component';
import { FooterComponent } from './shared/footer/footer.component';
import { PricingComponent } from './pricing/pricing.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { TermsOfUseComponent } from './terms-of-use/terms-of-use.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { HttpErrorHandler } from './http-error-handler.service';
import { MessageService } from './message.service';
import { MyGiveawaysComponent } from './my-giveaways/my-giveaways.component';
import { MyPackagesComponent } from './my-packages/my-packages.component';
import { MyProfileComponent } from './my-profile/my-profile.component';
import { UsersComponent } from './admin/users/users.component';
import { RafflesComponent } from './admin/raffles/raffles.component';
import { PackageSalesComponent } from './admin/package-sales/package-sales.component';
import { UserDetailComponent } from './admin/user-detail/user-detail.component';
import { UserRafflesComponent } from './admin/user-raffles/user-raffles.component';
import { UserPackagesComponent } from './admin/user-packages/user-packages.component';
import { UserMessagesComponent } from './admin/user-messages/user-messages.component';
import { CreateGiveawayComponent } from './create-giveaway/create-giveaway.component';
import { GiveawayComponent } from './giveaway/giveaway.component';
import { DetailComponent } from './detail/detail.component';
import { CekiComponent } from './ceki/ceki.component';
import { SafePipe } from './safe.pipe';
import { PaymentComponent } from './payment/payment.component';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavigationComponent,
    FooterComponent,
    PricingComponent,
    AboutUsComponent,
    ContactUsComponent,
    LoginComponent,
    RegisterComponent,
    TermsOfUseComponent,
    PrivacyPolicyComponent,
    MyGiveawaysComponent,
    MyPackagesComponent,
    MyProfileComponent,
    UsersComponent,
    RafflesComponent,
    PackageSalesComponent,
    UserDetailComponent,
    UserRafflesComponent,
    UserPackagesComponent,
    UserMessagesComponent,
    CreateGiveawayComponent,
    GiveawayComponent,
    DetailComponent,
    CekiComponent,
    SafePipe,
    PaymentComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    AppConfigModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
    }),
    AgGridModule.withComponents([]),
    MomentModule
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    HttpErrorHandler,
    MessageService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
