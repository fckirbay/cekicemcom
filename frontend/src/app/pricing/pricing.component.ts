import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser'
import { Router } from '@angular/router';

import { PricingService } from './pricing.service';


@Component({
  selector: 'app-pricing',
  templateUrl: './pricing.component.html',
  providers: [PricingService],
  styleUrls: ['./pricing.component.css']
})
export class PricingComponent implements OnInit {

  packages: any = [];
  package = '2K';
  isPayment = false;
  @ViewChild('element') public viewElement: ElementRef;
  public element: any;
  isLogged = false;

  constructor(private router: Router, private pricingService: PricingService, private sanitized: DomSanitizer) { }

  ngOnInit() {
    this.isLogged = this.loggedIn;
    this.getPackages();
  }

  public get loggedIn(): boolean{
    return localStorage.getItem('access_token') !==  null;
  }

  ngAfterViewInit() {
    
  }

  choosePackage(pack: any) {
    this.package = pack;
  }

  startPayment(pack: any, count: any) {
    if(this.isLogged == false) {
      this.router.navigate(['/register']);
    } else {
      if(this.viewElement) {
        this.viewElement.nativeElement.innerHTML = '';
      }
      this.isPayment = true;
      this.pricingService
        .postPayment({'package': pack, 'count': count})
        .subscribe((response: any) => {
          if(!response.error) {
            this.element = this.viewElement.nativeElement;
            const fragment = document.createRange().createContextualFragment(response.data);
            this.element.appendChild(fragment);
            /*setTimeout( () => { const element: any = document.getElementById('scroll-field');
            element.scrollIntoView({ behavior: 'smooth', block: 'end', inline: 'nearest' }); }, 1500 );*/
          } else {
            alert("Bir hata oluştu, lütfen tekrar deneyin!");
          }
        });
    }
    
  }

  getPackages(): void {
    this.pricingService.getPackages()
      .subscribe((pricing: any) => {
        this.packages = pricing.data;
      });
  }

}
