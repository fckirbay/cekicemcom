import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';


import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Pricing } from './models/pricing';
import { HttpErrorHandler, HandleError } from '../http-error-handler.service';

import { APP_CONFIG, AppConfig } from '../app-config.module';

@Injectable()
export class PricingService {
  
  private handleError: HandleError;

  constructor(
    private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig,
    httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('HomeService');
  }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer '+localStorage.getItem('access_token') || ''
    })
  };

  /** GET stats from the server */
  getPackages(): Observable<Pricing[]> {
    return this.http.get<Pricing[]>(this.config.apiEndpoint+'/packages')
      .pipe(
        catchError(this.handleError('getPackages', []))
      );
  }

  /** POST: add a new hero to the database */
  postPayment(packageInfos: any) {
    return this.http.post(this.config.apiEndpoint+'/payment', packageInfos, this.httpOptions)
      .pipe(
        catchError(this.handleError('paymentInfos'))
      );
  }

}
