import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(public translate: TranslateService, private router: Router) {
    translate.addLangs(['en', 'nl', 'tr']);
    translate.setDefaultLang(localStorage.getItem('lang') || 'tr');
  }
  title = 'instagram';

  /*ngOnInit() {
    this.router.events.subscribe(event => {
        if (event instanceof NavigationEnd) {
          const node2 = document.createElement('script');
            node2.src = './assets/js/lib.min.js';
            node2.type = 'text/javascript';
            node2.async = false;
            node2.id = 'lib.min';
            node2.charset = 'utf-8';
            document.getElementsByTagName('head')[0].appendChild(node2);
            
            const node = document.createElement('script');
            node.src = './assets/js/dashcore.min.js';
            node.type = 'text/javascript';
            node.async = false;
            node.id = 'dashcore.min';
            node.charset = 'utf-8';
            document.getElementsByTagName('head')[0].appendChild(node);

            
        }
    });
}*/
}
