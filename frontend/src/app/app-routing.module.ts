import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from './service/auth-guard.service';
import { HomeComponent } from './home/home.component';
import { PricingComponent } from './pricing/pricing.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { TermsOfUseComponent } from './terms-of-use/terms-of-use.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { MyGiveawaysComponent } from './my-giveaways/my-giveaways.component';
import { MyPackagesComponent } from './my-packages/my-packages.component';
import { MyProfileComponent } from './my-profile/my-profile.component';
import { CreateGiveawayComponent } from './create-giveaway/create-giveaway.component';
import { GiveawayComponent } from './giveaway/giveaway.component';
import { DetailComponent } from './detail/detail.component';
import { PaymentComponent } from './payment/payment.component';
import { CekiComponent } from './ceki/ceki.component';
import { UsersComponent } from './admin/users/users.component';
import { RafflesComponent } from './admin/raffles/raffles.component';
import { PackageSalesComponent } from './admin/package-sales/package-sales.component';
import { UserDetailComponent } from './admin/user-detail/user-detail.component';
import { UserRafflesComponent } from './admin/user-raffles/user-raffles.component';
import { UserPackagesComponent } from './admin/user-packages/user-packages.component';
import { UserMessagesComponent } from './admin/user-messages/user-messages.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'pricing', component: PricingComponent },
  { path: 'about-us', component: AboutUsComponent },
  { path: 'contact-us', component: ContactUsComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'terms-of-use', component: TermsOfUseComponent },
  { path: 'privacy-policy', component: PrivacyPolicyComponent },
  { path: 'my-giveaways', component: MyGiveawaysComponent, canActivate: [AuthGuardService] },
  { path: 'my-packages', component: MyPackagesComponent, canActivate: [AuthGuardService] },
  { path: 'my-profile', component: MyProfileComponent, canActivate: [AuthGuardService] },
  { path: 'create-giveaway', component: CreateGiveawayComponent, canActivate: [AuthGuardService] },
  { path: 'giveaway/:code', component: GiveawayComponent, canActivate: [AuthGuardService] },
  { path: 'payment', component: PaymentComponent },
  { path: 'ceki', component: CekiComponent, canActivate: [AuthGuardService] },
  { path: 'admin/users', component: UsersComponent, canActivate: [AuthGuardService] },
  { path: 'admin/raffles', component: RafflesComponent, canActivate: [AuthGuardService] },
  { path: 'admin/package-sales', component: PackageSalesComponent, canActivate: [AuthGuardService] },
  { path: 'admin/user-detail/:id', component: UserDetailComponent, canActivate: [AuthGuardService] },
  { path: 'admin/user-raffles/:id', component: UserRafflesComponent, canActivate: [AuthGuardService] },
  { path: 'admin/user-packages/:id', component: UserPackagesComponent, canActivate: [AuthGuardService] },
  { path: 'admin/user-messages/:id', component: UserMessagesComponent, canActivate: [AuthGuardService] },
  { path: ':code', component: DetailComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true, onSameUrlNavigation: 'reload' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
