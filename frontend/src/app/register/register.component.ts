import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { RegisterService } from './register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  providers: [RegisterService],
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm;
  tokenInfos: any;
  requiredFields = false;
  errorMessage: String = "";

  constructor(private registerService: RegisterService, private formBuilder: FormBuilder, private router: Router) {
    this.registerForm = this.formBuilder.group({
      name_surname: ['', Validators.required],
      email: ['', Validators.email],
      phone_number: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    
  }

  onSubmit(userData: any) {    
    if(this.registerForm.status == 'INVALID') {
      this.requiredFields = true;
    } else {
      this.requiredFields = false;
      if(userData.password.name_surname < 6) {
        this.errorMessage = "İsim Soyisim 6 karakterden az olamaz!";
      } else if(userData.password.length < 6) {
        this.errorMessage = "Şifreniz en az 6 karakterden oluşmalıdır!";
      } else if(userData.password.length > 15) {
        this.errorMessage = "Şifreniz en fazla 15 karakterden oluşmalıdır!";
      } else if(userData.password.email > 100) {
        this.errorMessage = "E-Posta adresiniz en fazla 100 karakterden oluşmalıdır!";
      } else if(userData.password.search(/[a-z]/) < 0) {
        this.errorMessage = "Şifreniz en az 1 küçük harf içermelidir!";
      } else if(userData.password.search(/[A-Z]/) < 0) {
        this.errorMessage = "Şifreniz en az 1 büyük harf içermelidir!";
      } else if(userData.password.search(/[0-9]/) < 0) {
        this.errorMessage = "Şifreniz en az 1 rakam içermelidir!";
      } else if(userData.phone_number.length < 10 || userData.phone_number.length > 11) {
        this.errorMessage = "Telefon numarası 10 ya da 11 rakamdan oluşmalıdır!";
      } else {
        // The server will generate the id for this new hero
        this.registerService
        .setRegister(userData)
        .subscribe(tokenInfos => {
          if(!tokenInfos.errorCode) {
            localStorage.setItem('access_token', tokenInfos.accessToken);
            this.router.navigate(['/']);
          } else {
            if(tokenInfos.errorCode == 104) {
              this.errorMessage = "Bu E-Posta adresi sistemimizde kayıtlıdır. Giriş yapmayı deneyebilirsiniz.";
            } else {
              this.errorMessage = "Sistemsel bir hata oluştu. Lütfen bizimle iletişime geçin, ya da bir süre sonra tekrar deneyin!";
            }
          }
        });
      }
    }
  }

}
