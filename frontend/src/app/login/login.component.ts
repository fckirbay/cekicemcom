import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  providers: [LoginService],
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm;
  tokenInfos: any;
  userNotFound: any;
  userBlocked: any;

  constructor(private loginService: LoginService, private formBuilder: FormBuilder, private router: Router) {
    this.loginForm = this.formBuilder.group({
      email: '',
      password: ''
    });
  }

  ngOnInit(): void {
  }

  onSubmit(userData: any) {    
    // The server will generate the id for this new hero
    this.loginService
      .setLogin(userData)
      .subscribe(tokenInfos => {
        if(tokenInfos.auth == false) {
          if(tokenInfos.reason == 'user_not_found') {
            this.userNotFound = true;
            this.userBlocked = false;
          } else if(tokenInfos.reason == 'user_blocked') {
            this.userNotFound = false;
            this.userBlocked = true;
          }
        } else {
          localStorage.setItem('access_token', tokenInfos.accessToken);
          this.router.navigate(['/']);
        }
      });
  }

}
