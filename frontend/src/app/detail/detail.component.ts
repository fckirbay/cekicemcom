import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { DetailService } from './detail.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  providers: [DetailService],
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  code: any;
  detail: any;
  winners: any;

  constructor(private actRoute: ActivatedRoute, private detailService: DetailService) { }

  ngOnInit(): void {
    this.actRoute.paramMap.subscribe(params => {
      this.code = params.get('code');
      this.getDetail(this.code);
    });
  }

  copyToClipboard(code: any) {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = code;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }

  getDetail(code: any): void {
    this.detailService.getDetail(code)
      .subscribe((resp: any) => {
        this.detail = resp.data;
        this.getWinners(this.detail.id);
      });
  }
  
  getWinners(giveaway_id: any): void {
    this.detailService.getWinners(giveaway_id)
      .subscribe((resp: any) => {
        this.winners = resp.data;
      });
  }

}
