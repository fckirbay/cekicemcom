import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

import { HttpErrorHandler, HandleError } from '../http-error-handler.service';

import { APP_CONFIG, AppConfig } from '../app-config.module';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
  })
};

@Injectable()
export class DetailService {
  private handleError: HandleError;

  constructor(
    private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig,
    httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('DetailService');
  }

  /** POST: add a new hero to the database */
  getDetail(code: any) {
    return this.http.get(this.config.apiEndpoint+'/detail?code='+code, httpOptions)
      .pipe(
        catchError(this.handleError('getDetail'))
      );
  }

  /** POST: add a new hero to the database */
  getWinners(giveaway_id: any) {
    return this.http.get(this.config.apiEndpoint+'/winners?giveaway_id='+giveaway_id, httpOptions)
      .pipe(
        catchError(this.handleError('getDetail'))
      );
  }
  
}
