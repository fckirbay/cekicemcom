import { Component, OnInit } from '@angular/core';

import { MyPackagesService } from './my-packages.service';

@Component({
  selector: 'app-my-packages',
  templateUrl: './my-packages.component.html',
  providers: [MyPackagesService],
  styleUrls: ['./my-packages.component.css']
})
export class MyPackagesComponent implements OnInit {
  
  packages: any;

  constructor(private myPackagesService: MyPackagesService) { }

  ngOnInit(): void {
    this.getMyPackages();
  }

  getMyPackages(): void {
    this.myPackagesService.getPackages()
      .subscribe((resp: any) => {
        this.packages = resp.data;
      });
  }

}
