import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CekiComponent } from './ceki.component';

describe('CekiComponent', () => {
  let component: CekiComponent;
  let fixture: ComponentFixture<CekiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CekiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CekiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
