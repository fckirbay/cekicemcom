import { Component, OnInit } from '@angular/core';

import { MyGiveawaysService } from './my-giveaways.service';

@Component({
  selector: 'app-my-giveaways',
  templateUrl: './my-giveaways.component.html',
  providers: [MyGiveawaysService],
  styleUrls: ['./my-giveaways.component.css']
})
export class MyGiveawaysComponent implements OnInit {

  giveaways: any;

  constructor(private myGiveawaysService: MyGiveawaysService) { }

  ngOnInit(): void {
    this.getMyGiveaways();
  }

  getMyGiveaways(): void {
    this.myGiveawaysService.getGiveaways()
      .subscribe((resp: any) => {
        this.giveaways = resp.data;
      });
  }

}
