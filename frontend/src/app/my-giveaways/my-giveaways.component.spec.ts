import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyGiveawaysComponent } from './my-giveaways.component';

describe('MyGiveawaysComponent', () => {
  let component: MyGiveawaysComponent;
  let fixture: ComponentFixture<MyGiveawaysComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyGiveawaysComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyGiveawaysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
