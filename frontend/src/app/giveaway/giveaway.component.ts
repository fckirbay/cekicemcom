import { collectExternalReferences } from '@angular/compiler';
import { Component, OnInit, NgZone } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { interval, Subscription} from 'rxjs';
import jwt_decode from 'jwt-decode';

import { GiveawayService } from './giveaway.service';
declare const annyang: any;

@Component({
  selector: 'app-giveaway',
  templateUrl: './giveaway.component.html',
  providers: [GiveawayService],
  styleUrls: ['./giveaway.component.css']
})
export class GiveawayComponent implements OnInit {

  code: any;
  giveaway: any;
  mySubscription: Subscription;
  randomSubscription: Subscription;
  counts: any;
  completePercent: any;
  doingGiveaway = false;
  winnerCount = 0;
  reverseWinnerCount = 0;
  winners: any = [];
  reserveWinners: any = [];
  randomComments: any = [];
  dummyUsername: any;
  dummyFullName: any;
  dummyProfilePicUrl: any;
  remainingTime = 10;
  countDown = 10;
  sound = true;
  sales: any;
  updateGiveawayForm: any;
  isInsufficient: boolean;
  downloadedComments = 0;
  followControlling = false;
  mediaInfoNew: any;

  voiceActiveSectionDisabled: boolean = true;
  voiceActiveSectionError: boolean = false;
  voiceActiveSectionSuccess: boolean = false;
  voiceActiveSectionListening: boolean = false;
  voiceText: any;
  urls: any = {};

  commentsFetching = false;

  constructor(private formBuilder: FormBuilder, private actRoute: ActivatedRoute, private giveawayService: GiveawayService, private ngZone: NgZone) {
    this.updateGiveawayForm = this.formBuilder.group({
      title: '',
      winner_count: 3,
      reserve_winner_count: 3,
      participation: 2,
      tag_count: 0,
      must_followed: '',
      package: '0'
    });
  }

  onUpdate(postData: any, code: any) {
    postData.giveaway_id = this.giveaway.id;
    this.isInsufficient = false;
    this.commentsFetching = true;

    this.urls['url'] = 'https://www.instagram.com/p/'+ this.giveaway.shortcode +'/';

    if(this.giveaway.shortcode_2) {
      this.urls['url2'] = 'https://www.instagram.com/p/'+ this.giveaway.shortcode_2 +'/';
    }
    if(this.giveaway.shortcode_3) {
      this.urls['url3'] = 'https://www.instagram.com/p/'+ this.giveaway.shortcode_3 +'/';
    }

    this.giveawayService
        .getMedia(this.urls)
        .subscribe(mediaInfo => {
          this.commentsFetching = false;
          if(mediaInfo.data) {
            this.mediaInfoNew = mediaInfo;
            this.giveaway.comment_count = mediaInfo.data.comment_count;
            this.giveaway.comment_count_2 = mediaInfo.data.comment_count_2;
            this.giveaway.comment_count_3 = mediaInfo.data.comment_count_3;
            this.giveaway.like_count = mediaInfo.data.like_count;

            postData.comment_count = mediaInfo.data.comment_count;
            postData.comment_count_2 = mediaInfo.data.comment_count_2;
            postData.comment_count_3 = mediaInfo.data.comment_count_3;
            postData.like_count = mediaInfo.data.like_count;
            
            /*this.giveawayService
            .updateGiveaway({ comment_count: mediaInfo.data.comment_count, like_count: mediaInfo.data.like_count, id: this.giveaway.id })
            .subscribe(updateInfo => {
              
            });*/
          }
          if(this.updateGiveawayForm.controls['package'].value == 0 && (this.giveaway.comment_count + this.giveaway.comment_count_2 + this.giveaway.comment_count_3) > 1000) {
            this.isInsufficient = true;
            setTimeout(()=>{                           //<<<---using ()=> syntax
              this.isInsufficient = false;
            }, 5000);
          } else {
            if(this.updateGiveawayForm.controls['package'].value == 0) {
              if((this.giveaway.comment_count + this.giveaway.comment_count_2 + this.giveaway.comment_count_3) > 1000) {
                this.isInsufficient = true;
                setTimeout(()=>{                           //<<<---using ()=> syntax
                  this.isInsufficient = false;
                }, 5000);
              } else {
                this.giveawayService
                  .postGiveaway(postData)
                  .subscribe(ga => {
                    this.giveawayService.fetchComments(code)
                      .subscribe((resp: any) => {
                        let elements = document.querySelectorAll('.update-form');
                        elements.forEach(element => {
                          element.setAttribute('disabled', 'true');
                        });
                        this.getGiveaway(code);
                    });
                })
              }
            } else {
              this.sales.forEach((sale : any, key :any) => {
                if(sale.package_id == this.updateGiveawayForm.controls['package'].value) {
                  if(sale.package.post_limit < (this.giveaway.comment_count + this.giveaway.comment_count_2 + this.giveaway.comment_count_3)) {
                    this.isInsufficient = true;
                    setTimeout(()=>{                           //<<<---using ()=> syntax
                      this.isInsufficient = false;
                    }, 5000);
                  } else {
                    this.giveawayService
                      .postGiveaway(postData)
                      .subscribe(ga => {
                        this.giveawayService.fetchComments(code)
                          .subscribe((resp: any) => {
                            let elements = document.querySelectorAll('.update-form');
                            elements.forEach(element => {
                              element.setAttribute('disabled', 'true');
                            });
                            this.getGiveaway(code);
                        });
                    })
                  }
                }
              });
            }
          }
      });


    
  }

  initializeVoiceRecognitionCallback(): void {
    annyang.addCallback('error', (err: any) => {
      if(err.error === 'network'){
        this.voiceText = "Internet is require";
        annyang.abort();
        this.ngZone.run(() => this.voiceActiveSectionSuccess = true);
      } else if (this.voiceText === undefined) {
        this.ngZone.run(() => this.voiceActiveSectionError = true);
        //annyang.abort();
      }
    });
   annyang.addCallback('soundstart', (res: any) => {
    this.ngZone.run(() => this.voiceActiveSectionListening = true);
   });
   annyang.addCallback('end', () => {
     if (this.voiceText === undefined) {
       this.ngZone.run(() => this.voiceActiveSectionError = true);
       //annyang.abort();
     }
   });
   annyang.addCallback('result', (userSaid: any) => {
     this.ngZone.run(() => this.voiceActiveSectionError = false);
     let queryText: any = userSaid[0];
     //annyang.abort();
     this.voiceText = queryText;
     this.ngZone.run(() => this.voiceActiveSectionListening = false);
     this.ngZone.run(() => this.voiceActiveSectionSuccess = true);
   });
 }
 startVoiceRecognition(): void {
   this.voiceActiveSectionDisabled = false;
   this.voiceActiveSectionError = false;
   this.voiceActiveSectionSuccess = false;
   this.voiceText = undefined;
   if (annyang) {
     annyang.setLanguage('tr-TR');
     let commands = {
       'çekilişi başlat': () => {
        var name = this.getDecodedAccessToken(localStorage.getItem('access_token')).name_surname.split(' ')[0];
        if(this.sound) {
          this.giveawayService
          .setSpeech({'text': 'Tamam '+ name + '. Çekilişi başlatıyorum. Herkese bol şanslar.'})
          .subscribe((url: any) => {
            this.playAudio(url.data);
            setTimeout( () => { 
              let element: HTMLElement = document.getElementById('start-giveaway') as HTMLElement;
              element.click();
            }, 2000 );
          });
        } else {
          let element: HTMLElement = document.getElementById('start-giveaway') as HTMLElement;
          element.click();
        }
       },
       'çekilişi tekrarla': () => {
         if(this.sound) {
          var name = this.getDecodedAccessToken(localStorage.getItem('access_token')).name_surname.split(' ')[0];
          this.giveawayService
          .setSpeech({'text': 'Tamam '+ name + '. Çekilişi tekrarlıyorum. Herkese bol şanslar.'})
          .subscribe((url: any) => {
            this.playAudio(url.data);
            setTimeout( () => { 
              let element: HTMLElement = document.getElementById('again-giveaway') as HTMLElement;
              element.click();
            }, 2000 );
          });
         } else {
          let element: HTMLElement = document.getElementById('again-giveaway') as HTMLElement;
          element.click();
         }
        
        
       },
       'Çeki seni seviyorum': () => {
        if(this.sound) {
          var name = this.getDecodedAccessToken(localStorage.getItem('access_token')).name_surname.split(' ')[0];
          this.giveawayService
          .setSpeech({'text': 'Teşekkürler '+ name + '. Ben de seni çok seviyorum.'})
          .subscribe((url: any) => {
            this.playAudio(url.data);
          });
        }
       }
     };
     annyang.addCommands(commands);
    
     this.initializeVoiceRecognitionCallback();
     annyang.start({ autoRestart: true, continuous: true });
   }
 }
 closeVoiceRecognition(): void {
   this.voiceActiveSectionDisabled = true;
   this.voiceActiveSectionError = false;
   this.voiceActiveSectionSuccess = false;
   this.voiceActiveSectionListening = false;
   this.voiceText = undefined;
   if(annyang){
    annyang.abort();
   }
 }

  ngOnInit(): void {
    this.actRoute.paramMap.subscribe(params => {
      this.code = params.get('code');
      this.getGiveaway(this.code);
    });
    this.getSales();
  }

  getSales(): void {
    this.giveawayService.getSales()
      .subscribe((sales: any) => {
        this.sales = sales.data;
      });
  }

  getDecodedAccessToken(token: any): any {
    try{
        return jwt_decode(token);
    }
    catch(Error){
        return null;
    }
  }

  ngOnDestroy(): void {
    if(this.mySubscription) {
      this.mySubscription.unsubscribe();
    }
    if(this.randomSubscription) {
      this.randomSubscription.unsubscribe();
    }
    if (annyang) {
      annyang.abort();
    }
  }

  getGiveaway(code: any): void {
    this.giveawayService.getGiveaway(code, false)
      .subscribe((resp: any) => {
        if(resp.data && resp.data.length > 0) {
          this.giveaway = resp.data[0];
          this.updateGiveawayForm.controls['title'].setValue(this.giveaway.title);
          this.updateGiveawayForm.controls['winner_count'].setValue(this.giveaway.winner_count);
          this.updateGiveawayForm.controls['reserve_winner_count'].setValue(this.giveaway.reserve_winner_count);
          this.updateGiveawayForm.controls['participation'].setValue(parseInt(this.giveaway.participation));
          this.updateGiveawayForm.controls['tag_count'].setValue(this.giveaway.tag_count);
          this.updateGiveawayForm.controls['must_followed'].setValue(this.giveaway.must_followed);
          this.getWinners(this.giveaway.id);
          this.completePercent = (this.giveaway.request_count * 100 / Math.ceil((this.giveaway.comment_count + this.giveaway.comment_count_2 + this.giveaway.comment_count_3)/50)).toFixed(2);
          if(this.completePercent > 100) {
            this.completePercent = 100;
          }
          this.counts = { 'request_count': this.giveaway.request_count, 'comment_count': this.giveaway.comment_count, 'comment_count_2': this.giveaway.comment_count_2, 'comment_count_3': this.giveaway.comment_count_3, 'downloaded_comments': this.giveaway.request_count * 50 };
          console.log(this.giveaway.status, this.giveaway.comment_count , this.giveaway.downloaded_comment_count);
          if(this.giveaway.status == 1 && this.giveaway.comment_count > this.giveaway.downloaded_comment_count) {
            this.mySubscription= interval(1000).subscribe((x =>{
              if(this.counts.request_count < Math.ceil((this.counts.comment_count+this.counts.comment_count_2+this.counts.comment_count_3)/50)) {
                this.getCounts(this.code);
              } else {
                this.mySubscription.unsubscribe();
              }
            }));
          } else if(this.giveaway.status == 2 || this.completePercent >= 100 || this.giveaway.status == 3) {
            this.startVoiceRecognition();
            let elements = document.querySelectorAll('.update-form');
              elements.forEach(element => {
              element.setAttribute('disabled', 'true');
            });
          }
        }
      });
  }

  copyToClipboard(code: any) {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = code;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }

  getWinners(id: any) {
    this.giveawayService.getWinners(id)
      .subscribe((resp: any) => {
        resp.data.forEach((keys : any, vals :any) => {
          var isFollowChecked = 0;
          if(keys.type == 1) {
            if(keys.follow_control == null) {
              keys.follow = false;
            } else {
              keys.follow = true;
              var obj = this.json2array(JSON.parse(keys.follow_control));
              keys.follow_control = obj;
              obj.forEach((element: any) => {
                if(element.status > 0) {
                  isFollowChecked++;
                }
              });
              if(isFollowChecked > 0) {
                keys.follow = false;
              }
            }
            this.winners.push(keys);
          } else {
            if(keys.follow_control == null) {
              keys.follow = false;
            } else {
              keys.follow = true;
              var obj = this.json2array(JSON.parse(keys.follow_control));
              keys.follow_control = obj;
              obj.forEach((element: any) => {
                if(element.status > 0) {
                  isFollowChecked++;
                }
              });
              if(isFollowChecked > 0) {
                keys.follow = false;
              }
            }
            this.reserveWinners.push(keys);
          }
        })
      });
  }

   json2array(json: any){
    var result: any = [];
    var keys = Object.keys(json);
    keys.forEach(function(key){
        result.push({ username: key, status: json[key]});
    });
    return result;
}

  changeGender(a:any) {
    console.log("değişti", a);
  }

  readCode(code: any) {
    this.giveawayService
      .setSpeech({'text': 'Çekiliş kodunuz: ' + code})
      .subscribe((url: any) => {
        this.playAudio(url.data);
      });
  }

  getCounts(code: any): void {
    this.giveawayService.getGiveaway(code, true)
      .subscribe((resp: any) => {
        if(resp.data && resp.data.length > 0) {
          this.counts = resp.data[0];
          this.completePercent = (this.counts.request_count * 100 / Math.ceil((this.counts.comment_count + this.counts.comment_count_2 + this.counts.comment_count_3)/50)).toFixed(2);
          this.counts.downloaded_comments = this.counts.request_count * 50;
          if(this.completePercent > 100) {
            this.completePercent = 100;
          }
        }
      });
  }

  fetchComments(code: any) {
    //onupdate kısmında yapılıyor.
  }

  showResults() {
    this.doingGiveaway = true;
  }

  hideResults() {
    this.doingGiveaway = false;
  }

  changeWinnerStatus(code: any, order_no: any, status: any, type: any, winner_status: any) {
    if(status != winner_status) {
      this.giveawayService
      .setWinnerStatus({'code': code, 'order_no': order_no, 'status': status, 'type': type})
      .subscribe(response => {
        this.winners.forEach((keys : any, vals :any) => {
          if (keys.order_no == order_no && keys.type == type) {
              keys.status = status;
          }
        })
        this.reserveWinners.forEach((keys : any, vals :any) => {
          if (keys.order_no == order_no && keys.type == type) {
              keys.status = status;
          }
        })
      });
    }
  }

  startFollowControl(code: any, owner_id: any) {
    this.followControlling = true;
    this.giveawayService
      .startFollowControl(code, owner_id)
      .subscribe((result: any) => {
        this.winners.forEach((keys : any, vals :any) => {
          if (keys.owner_id == owner_id) {
              keys.follow = false;
              keys.follow_control = result.data;
          }
        })
        this.reserveWinners.forEach((keys : any, vals :any) => {
          if (keys.owner_id == owner_id) {
            keys.follow = false;
            keys.follow_control = result.data;
          }
        })
        this.followControlling = false;
    })
  }

  playAudio(url: any) {
    let audio = new Audio();
    audio.src = "data:audio/wav;base64,"+url;
    audio.load();
    audio.playbackRate = 1.25;
    audio.play();
  }
  

  doGiveaway(code: any) {
    if(this.mySubscription) {
      this.mySubscription.unsubscribe();
    }
    if(this.randomSubscription) {
      this.randomSubscription.unsubscribe();
    }
    if (annyang) {
      annyang.abort();
    }
    this.giveaway.status = 3;
    this.winners = [];
    this.reserveWinners = [];
    this.doingGiveaway = true;
    this.giveawayService.getRandomComments(code)
      .subscribe((resp2: any) => {
        this.randomComments = resp2.data.comments;
        var countTimer = 0;
        this.randomSubscription= interval(100).subscribe((x =>{
          var rand = Math.floor(Math.random() * this.randomComments.length);
          if(this.randomComments[rand]) {
            countTimer++;
            this.dummyUsername = this.randomComments[rand].username;
            this.dummyFullName = this.randomComments[rand].full_name;
            this.dummyProfilePicUrl = this.randomComments[rand].profile_pic_url;
            if(countTimer % 10 == 0) {
              this.remainingTime--;
              if(this.remainingTime == 0) {
                this.remainingTime = this.countDown;
              }
            }
          } else {
            this.dummyUsername = '';
            this.dummyFullName = '';
            this.dummyProfilePicUrl = '';
          }
        }));
        this.giveawayService.doGiveaway(code)
          .subscribe((resp: any) => {
            resp.data.winners.forEach((keys : any, vals :any) => {
              var time = (vals+1) * this.countDown * 1000;
              setTimeout(() => {
                var isFollowChecked = 0;
                if(this.giveaway.must_followed == null) {
                  keys.follow = false;
                } else {
                  keys.follow = true;
                }
                console.log("keys", keys);
                if(keys.type == 1) {
                  this.winners.push(keys);
                  if(this.sound) {
                    this.giveawayService
                    .setSpeech({'text': keys.order_no + '. asil kazanan: '+ keys.username + '!'})
                    .subscribe((url: any) => {
                      this.playAudio(url.data);
                    });
                  }
                  
                } else {
                  this.reserveWinners.push(keys);
                  if(this.sound) {
                    this.giveawayService
                    .setSpeech({'text': keys.order_no + '. yedek kazanan: '+ keys.username + '!'})
                    .subscribe((url: any) => {
                      this.playAudio(url.data);
                    });
                  }
                  
                }
              }, time);
            })
          });
        });
  }

}
