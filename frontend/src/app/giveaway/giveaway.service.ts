import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

import { HttpErrorHandler, HandleError } from '../http-error-handler.service';

import { APP_CONFIG, AppConfig } from '../app-config.module';



@Injectable()
export class GiveawayService {
  private handleError: HandleError;

  constructor(
    private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig,
    httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('HomeService');
  }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer '+localStorage.getItem('access_token') || ''
    })
  };

  /** POST: add a new hero to the database */
  getGiveaway(code: any, type: any) {
    if(type == false) {
      return this.http.get(this.config.apiEndpoint+'/giveaway?code='+code, this.httpOptions)
        .pipe(
          catchError(this.handleError('getGiveaway'))
        );
    } else {
      return this.http.get(this.config.apiEndpoint+'/giveaway?code='+code+'&fields=comment_count,comment_count_2,comment_count_3,request_count', this.httpOptions)
        .pipe(
          catchError(this.handleError('getGiveaway'))
        );
    }
    
  }

  /** POST: add a new hero to the database */
  getSales() {
    return this.http.get(this.config.apiEndpoint+'/sales', this.httpOptions)
      .pipe(
        catchError(this.handleError('getSales'))
      );
  }

  /** POST: add a new hero to the database */
  fetchComments(code: any) {
    return this.http.get(this.config.apiEndpoint+'/ck/fetchcomments?code='+code, this.httpOptions)
      .pipe(
        catchError(this.handleError('fetchComments'))
      );
  }

  /** POST: add a new hero to the database */
  getWinners(id: any) {
    return this.http.get(this.config.apiEndpoint+'/winners?giveaway_id='+id, this.httpOptions)
      .pipe(
        catchError(this.handleError('getWinners'))
      );
  }

  /** POST: add a new hero to the database */
  startFollowControl(code: any, owner_id: any) {
    return this.http.post(this.config.apiEndpoint+'/ck/followcontrol', {'code': code, 'owner_id': owner_id}, this.httpOptions)
      .pipe(
        catchError(this.handleError('followControl'))
      );
  }

  /** POST: add a new hero to the database */
  doGiveaway(code: any) {
    return this.http.get(this.config.apiEndpoint+'/ck/dogiveaway?code='+code, this.httpOptions)
      .pipe(
        catchError(this.handleError('doGiveaway'))
      );
  }

  /** POST: add a new hero to the database */
  getRandomComments(code: any) {
    return this.http.get(this.config.apiEndpoint+'/ck/randomcomments?code='+code, this.httpOptions)
      .pipe(
        catchError(this.handleError('getRandomComments'))
      );
  }

  /** POST: add a new hero to the database */
  setWinnerStatus(winnerInfos: any) {
    return this.http.post(this.config.apiEndpoint+'/ck/winnerstatus', winnerInfos, this.httpOptions)
      .pipe(
        catchError(this.handleError('winnerInfos'))
      );
  }

  setSpeech(speechText: any) {
    return this.http.post(this.config.apiEndpoint+'/speech', speechText, this.httpOptions)
      .pipe(
        catchError(this.handleError('speechText'))
      );
  }

  /** POST: add a new hero to the database */
  postGiveaway(giveawayData: any) {
    return this.http.post(this.config.apiEndpoint+'/giveaway', giveawayData, this.httpOptions)
      .pipe(
        catchError(this.handleError('postGiveaway', giveawayData))
      );
  }

  /** POST: add a new hero to the database */
  getMedia(mediaData: any) {
    return this.http.post(this.config.apiEndpoint+'/instapost/get', mediaData, this.httpOptions)
      .pipe(
        catchError(this.handleError('getMedia', mediaData))
      );
  }

  /** POST: add a new hero to the database */
  updateGiveaway(giveawayData: any) {
    return this.http.put(this.config.apiEndpoint+'/giveaway', giveawayData, this.httpOptions)
      .pipe(
        catchError(this.handleError('updateGiveaway', giveawayData))
      );
  }
  
}
