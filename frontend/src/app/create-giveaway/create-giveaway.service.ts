import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

import { HttpErrorHandler, HandleError } from '../http-error-handler.service';

import { APP_CONFIG, AppConfig } from '../app-config.module';



@Injectable()
export class CreateGiveawayService {
  private handleError: HandleError;

  constructor(
    private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig,
    httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('HomeService');
  }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer '+localStorage.getItem('access_token') || ''
    })
  };

  /** POST: add a new hero to the database */
  getSales() {
    return this.http.get(this.config.apiEndpoint+'/sales', this.httpOptions)
      .pipe(
        catchError(this.handleError('getSales'))
      );
  }
  
  /** POST: add a new hero to the database */
  getMedia(mediaData: any) {
    return this.http.post(this.config.apiEndpoint+'/instapost/get', mediaData, this.httpOptions)
      .pipe(
        catchError(this.handleError('getMedia', mediaData))
      );
  }

  /** POST: add a new hero to the database */
  postGiveaway(giveawayData: any) {
    return this.http.post(this.config.apiEndpoint+'/giveaway', giveawayData, this.httpOptions)
      .pipe(
        catchError(this.handleError('postGiveaway', giveawayData))
      );
  }
}
