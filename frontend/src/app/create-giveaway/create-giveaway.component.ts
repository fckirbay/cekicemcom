import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

import { CreateGiveawayService } from './create-giveaway.service';

@Component({
  selector: 'app-create-giveaway',
  templateUrl: './create-giveaway.component.html',
  providers: [CreateGiveawayService],
  styleUrls: ['./create-giveaway.component.css']
})
export class CreateGiveawayComponent implements OnInit {

  // https://www.instagram.com/p/CJmWI02gPLa/
  isLoading = false;
  postUrlForm: any;
  createGiveawayForm: any;
  //Default boolean FALSE
  isPost = false;
  //Default any NULL
  mediaInfo: any;
  sales: any;
  isInsufficient = false;
  tagCount = 0;
  errorBox = false;
  postCount = 1;
  
  
  constructor(private formBuilder: FormBuilder, private giveawayService: CreateGiveawayService, private router: Router) {
    this.postUrlForm = this.formBuilder.group({
      url: '',
      url2: '',
      url3: ''
    });
    this.createGiveawayForm = this.formBuilder.group({
      title: '',
      winner_count: 3,
      reserve_winner_count: 3,
      participation: 2,
      tag_count: '',
      must_followed: '',
      package: 0
    });
  }

  ngOnInit(): void {
    this.getSales();
    /*this.mediaInfo = {
      "data": {
          "shortcode": "CJmWI02gPLa",
          "display_url": "https://instagram.fadb3-2.fna.fbcdn.net/v/t51.2885-15/e35/135042662_422795248771231_4528297830315899908_n.jpg?_nc_ht=instagram.fadb3-2.fna.fbcdn.net&_nc_cat=1&_nc_ohc=aelPKqTSgrEAX9LbwGq&tp=1&oh=bf74cccf9dd7bdeed60b750f8f530333&oe=601DFA16",
          "comment_count": 33432,
          "like_count": 5850667,
          "owner_id": "173560420",
          "owner_is_verified": true,
          "owner_profile_pic_url": "https://instagram.fadb3-2.fna.fbcdn.net/v/t51.2885-19/s150x150/67310557_649773548849427_4130659181743046656_n.jpg?_nc_ht=instagram.fadb3-2.fna.fbcdn.net&_nc_ohc=9JunhZETV4oAX929ICN&tp=1&oh=f488b4f649454c7970695325240ee033&oe=601D1FD3",
          "owner_username": "cristiano",
          "owner_full_name": "Cristiano Ronaldo",
          "owner_timeline_media_count": 2980,
          "owner_follower_count": 251281495,
          "is_ad": false,
          "caption": "Grandi ragazzi!👏🏽\nAbbiamo iniziato il 2021 nel migliore dei modi! 🏳️🏴💪🏽 #finoallafine",
          "datetime": "1609748708"
        }
      };
      */
  }

  getSales(): void {
    this.giveawayService.getSales()
      .subscribe((sales: any) => {
        this.sales = sales.data;
      });
  }

  onSubmit(postData: any) {
    if(!localStorage.getItem('access_token')) {
      this.errorBox = true;
    } else {
      this.errorBox = false;
      this.isLoading = true;
      // The server will generate the id for this new hero
      
      this.giveawayService
        .getMedia(postData)
        .subscribe(mediaInfo => {
          this.isLoading = false;
          if(!mediaInfo.data) {
            alert("Post çekilemedi!");
          } else {
            this.isPost = true;
            this.mediaInfo = mediaInfo;
          }
      });
    }
    
  }

  addNewPost() {
    this.postCount++;
  }

  removeNewPost() {
    this.postCount--;
  }

  onCreate(postData: any) {
    postData.mediaInfo = this.mediaInfo.data;
    this.giveawayService
      .postGiveaway(postData)
      .subscribe(ga => {
        this.router.navigate(['giveaway', ga.code]);
      })
    /*
    this.isInsufficient = false;
    if(postData.package == 0 && this.mediaInfo.data.comment_count > 1000) {
      this.isInsufficient = true;
    } else {
      if(postData.package == 0) {
        if(this.mediaInfo.data.comment_count > 1000) {
          this.isInsufficient = true;
        } else {
          postData.mediaInfo = this.mediaInfo.data;
          this.giveawayService
            .postGiveaway(postData)
            .subscribe(ga => {
              this.router.navigate(['giveaway', ga.code]);
            })
        }
      } else {
        this.sales.forEach((sale : any, key :any) => {
          if(sale.id == postData.package) {
            if(sale.package.post_limit < this.mediaInfo.data.comment_count) {
              this.isInsufficient = true;
            } else {
              postData.mediaInfo = this.mediaInfo.data;
              this.giveawayService
                .postGiveaway(postData)
                .subscribe(ga => {
                  this.router.navigate(['giveaway', ga.code]);
                })
            }
          }
        });
      }
    }
    */

    
  }

}
