import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';


import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Stats } from './models/stats';
import { Upcomings } from './models/upcomings';
import { HttpErrorHandler, HandleError } from '../http-error-handler.service';

import { APP_CONFIG, AppConfig } from '../app-config.module';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    Authorization: 'my-auth-token'
  })
};

@Injectable()
export class HomeService {
  private handleError: HandleError;

  constructor(
    private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig,
    httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('HomeService');
  }

  /** GET stats from the server */
  getStats(): Observable<Stats[]> {
    return this.http.get<Stats[]>(this.config.apiEndpoint+'/stats')
      .pipe(
        catchError(this.handleError('getStats', []))
      );
  }

  /** GET stats from the server */
  getUpcomings(): Observable<Upcomings[]> {
    return this.http.get<Upcomings[]>(this.config.apiEndpoint+'/upcomings')
      .pipe(
        catchError(this.handleError('getUpcomings', []))
      );
  }

  /* GET heroes whose name contains search term */
  searchStats(term: string): Observable<Stats[]> {
    term = term.trim();

    // Add safe, URL encoded search parameter if there is a search term
    const options = term ?
     { params: new HttpParams().set('name', term) } : {};

    return this.http.get<Stats[]>(this.config.apiEndpoint+'/stats', options)
      .pipe(
        catchError(this.handleError<Stats[]>('searchStats', []))
      );
  }

  //////// Save methods //////////

  /** POST: add a new hero to the database */
  addStats(stats: Stats): Observable<Stats> {
    return this.http.post<Stats>(this.config.apiEndpoint+'/stats', stats, httpOptions)
      .pipe(
        catchError(this.handleError('addHero', stats))
      );
  }

  /** DELETE: delete the hero from the server */
  /*deleteHero(id: number): {
    const url = '${this.statsUrl}/${id}'; // DELETE api/heroes/42
    return this.http.delete(url, httpOptions)
      .pipe(
        catchError(this.handleError('deleteHero'))
      );
  }*/

  /** PUT: update the hero on the server. Returns the updated hero upon success. */
  updateHero(stats: Stats): Observable<Stats> {
    httpOptions.headers =
      httpOptions.headers.set('Authorization', 'my-new-auth-token');

    return this.http.put<Stats>(this.config.apiEndpoint+'/stats', stats, httpOptions)
      .pipe(
        catchError(this.handleError('updateStats', stats))
      );
  }
}
