export interface Stats {
    user_cnt: number;
    raffle_cnt: number;
    winner_cnt: number;
    comment_cnt: number;
  }
  