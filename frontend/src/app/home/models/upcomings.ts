export interface Upcomings {
    username: String;
    caption: String;
    photo_url: String;
    date: Date;
    follower_cnt: number;
    comment_cnt: number;
    is_verified: boolean;
  }
  