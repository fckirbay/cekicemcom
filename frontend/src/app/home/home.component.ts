import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

import { HomeService } from './home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  providers: [HomeService],
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  detailForm: any;
  stats: any = [];
  upcomings: any = [];

  constructor(private router: Router, private formBuilder: FormBuilder, private homeService: HomeService) { }

  ngOnInit() {
    this.detailForm = this.formBuilder.group({
      code: ''
    });
    this.getStats();
    this.getUpcomings();
  }

  ngAfterViewInit() {
    
  }

  getStats(): void {
    this.homeService.getStats()
      .subscribe((stats: any) => {
        this.stats = stats.data;
      });
  }

  getUpcomings(): void {
    this.homeService.getUpcomings()
      .subscribe((upcomings: any) => {
        this.upcomings = upcomings.data;
      });
  }

  onSubmit(detail: any) {
    if(detail.code != "") {
      this.router.navigate(['/', detail.code]);
    } 
  }

}
