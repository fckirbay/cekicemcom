import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import jwt_decode from 'jwt-decode';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  isLogged = false;
  isAdmin = false;
  userName: any;

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.isLogged = this.loggedIn;
    if(this.isLogged == true) {
      this.userName = this.getDecodedAccessToken(localStorage.getItem('access_token')).name_surname.split(' ')[0];

      if(this.getDecodedAccessToken(localStorage.getItem('access_token')).isAdmin == 1) {
        this.isAdmin = true;
      }
    }
  }

  ngAfterViewInit() {
    let script = document.createElement('script');
    script.src = 'assets/js/dashcore.min.js';
    document.body.appendChild(script);
  }

  public get loggedIn(): boolean{
    return localStorage.getItem('access_token') !==  null;
  }

  logout() {
    localStorage.removeItem('access_token');
    this.router.navigate(['/']);
  }

  getDecodedAccessToken(token: any): any {
    try{
        return jwt_decode(token);
    }
    catch(Error){
        return null;
    }
  }

}
