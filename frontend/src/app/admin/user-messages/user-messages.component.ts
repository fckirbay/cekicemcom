import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { UserMessagesService } from './user-messages.service';

@Component({
  selector: 'app-user-messages',
  templateUrl: './user-messages.component.html',
  providers: [UserMessagesService],
  styleUrls: ['./user-messages.component.css']
})
export class UserMessagesComponent implements OnInit {

  userId: any;
  userDetail: any;
  messages:any = [];

  constructor(private actRoute: ActivatedRoute, private userDetailService: UserMessagesService) { }

  ngOnInit(): void {
    this.actRoute.paramMap.subscribe(params => {
      this.userId = params.get('id');
      this.getUserDetail(this.userId);
    });
  }

  getUserDetail(userId: any): void {
    this.userDetailService.getUserDetail(userId)
      .subscribe((user: any) => {
        if(user.data.length == 0) {
          this.userDetail = null;
        } else {
          this.userDetail = user.data[0];
        }
      });
  }

}
