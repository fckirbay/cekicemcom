import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';


import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Users } from './models/users';
import { HttpErrorHandler, HandleError } from '../../http-error-handler.service';

import { APP_CONFIG, AppConfig } from '../../app-config.module';

const headers = new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'Bearer '+localStorage.getItem('access_token') || ''
});


@Injectable()
export class UserDetailService {
  private handleError: HandleError;

  constructor(
    private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig,
    httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('UserDetailsService');
  }

  /* GET heroes whose name contains search term */
  getUserDetail(userId: string): Observable<Users[]> {
    userId = userId.trim();

    const params = new HttpParams().set('id', userId);

    return this.http.get<Users[]>(this.config.apiEndpoint+'/admin/users', { headers, params})
      .pipe(
        catchError(this.handleError<Users[]>('searchStats', []))
      );
  }

}
