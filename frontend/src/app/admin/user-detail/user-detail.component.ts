import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { UserDetailService } from './user-detail.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  providers: [UserDetailService],
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {

  userId: any;
  userDetail: any;

  constructor(private actRoute: ActivatedRoute, private userDetailService: UserDetailService) { }

  ngOnInit(): void {
    this.actRoute.paramMap.subscribe(params => {
      this.userId = params.get('id');
      this.getUserDetail(this.userId);
    });
  }

  getUserDetail(userId: any): void {
    this.userDetailService.getUserDetail(userId)
      .subscribe((user: any) => {
        if(user.data.length == 0) {
          this.userDetail = null;
        } else {
          this.userDetail = user.data[0];
        }
      });
  }

}
