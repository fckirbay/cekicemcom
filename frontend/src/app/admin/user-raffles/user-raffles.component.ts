import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { UserRafflesService } from './user-raffles.service';

@Component({
  selector: 'app-user-messages',
  templateUrl: './user-raffles.component.html',
  providers: [UserRafflesService],
  styleUrls: ['./user-raffles.component.css']
})
export class UserRafflesComponent implements OnInit {

  userId: any;
  userDetail: any;
  raffles:any = [];

  constructor(private actRoute: ActivatedRoute, private userRafflesService: UserRafflesService) { }

  ngOnInit(): void {
    this.actRoute.paramMap.subscribe(params => {
      this.userId = params.get('id');
      this.getUserDetail(this.userId);
    });
  }

  getUserDetail(userId: any): void {
    this.userRafflesService.getUserDetail(userId)
      .subscribe((user: any) => {
        if(user.data.length == 0) {
          this.userDetail = null;
        } else {
          this.userDetail = user.data[0];
        }
      });
  }

}
