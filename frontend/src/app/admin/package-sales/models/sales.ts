export interface Sales {
    id: String;
    user_id: String;
    package_id: String;
    remaining: String;
    createdAt: String;
    updatedAt: String;
  }
  