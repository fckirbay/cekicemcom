import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AgGridAngular } from 'ag-grid-angular';

import { SalesService } from './package-sales.service';

@Component({
  selector: 'app-users',
  templateUrl: './package-sales.component.html',
  providers: [SalesService],
  styleUrls: ['./package-sales.component.css']
})
export class PackageSalesComponent implements OnInit {

  @ViewChild('agGrid') agGrid?: AgGridAngular;

  sales: any = [];

  constructor(private router: Router, private salesService: SalesService) {
  }

  ngOnInit(): void {
    this.getSales();
  }
  
  getSales(): void {
    this.salesService.getSales()
      .subscribe((sales: any) => {
        this.sales = sales.data;
      });
  }


  onDoubleClick(row: any) {
    this.router.navigate(['/admin/sales', row.data.id]);
  }

}
