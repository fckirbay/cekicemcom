import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PackageSalesComponent } from './package-sales.component';

describe('PackageSalesComponent', () => {
  let component: PackageSalesComponent;
  let fixture: ComponentFixture<PackageSalesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PackageSalesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PackageSalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
