import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Sales } from './models/sales';
import { HttpErrorHandler, HandleError } from '../../http-error-handler.service';

import { APP_CONFIG, AppConfig } from '../../app-config.module';

const headers = new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'Bearer '+localStorage.getItem('access_token') || ''
});


@Injectable()
export class SalesService {
  private handleError: HandleError;

  constructor(
    private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig,
    httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('SalesService');
  }

  /* GET heroes whose name contains search term */
  getSales(): Observable<Sales[]> {

    return this.http.get<Sales[]>(this.config.apiEndpoint+'/admin/sales', { headers})
      .pipe(
        catchError(this.handleError<Sales[]>('getSales', []))
      );
  }

}
