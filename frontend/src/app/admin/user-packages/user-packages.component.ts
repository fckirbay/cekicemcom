import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { UserPackagesService } from './user-packages.service';

@Component({
  selector: 'app-user-messages',
  templateUrl: './user-packages.component.html',
  providers: [UserPackagesService],
  styleUrls: ['./user-packages.component.css']
})
export class UserPackagesComponent implements OnInit {

  userId: any;
  userDetail: any;
  packages:any = [];

  constructor(private actRoute: ActivatedRoute, private userPackagesService: UserPackagesService) { }

  ngOnInit(): void {
    this.actRoute.paramMap.subscribe(params => {
      this.userId = params.get('id');
      this.getUserDetail(this.userId);
    });
  }

  getUserDetail(userId: any): void {
    this.userPackagesService.getUserDetail(userId)
      .subscribe((user: any) => {
        if(user.data.length == 0) {
          this.userDetail = null;
        } else {
          this.userDetail = user.data[0];
        }
      });
  }

}
