export interface Users {
    email: String;
    name_surname: String;
    phone_number: String;
    lang: String;
    verification: String;
    createdAt: String;
    updatedAt: String;
  }
  