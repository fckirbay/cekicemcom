import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AgGridAngular } from 'ag-grid-angular';

import { UsersService } from './users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  providers: [UsersService],
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  @ViewChild('agGrid') agGrid?: AgGridAngular;

  users: any = [];
  gridApi: any;
  gridColumnApi: any;

  constructor(private router: Router, private usersService: UsersService) {
  }

  ngOnInit(): void {
    this.getUsers();
  }

  getUsers(): void {
    this.usersService.getUsers()
      .subscribe((users: any) => {
        this.users = users.data;
      });
  }

  getUserDetail(userId: any) {
    this.router.navigate(['/admin/user-detail', userId]);
  }

}
