import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Users } from './models/users';
import { HttpErrorHandler, HandleError } from '../../http-error-handler.service';

import { APP_CONFIG, AppConfig } from '../../app-config.module';

const headers = new HttpHeaders({
  'Content-Type':  'application/json',
  'Authorization': 'Bearer '+localStorage.getItem('access_token') || ''
});

@Injectable()
export class UsersService {
  private handleError: HandleError;

  constructor(
    private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig,
    httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('UsersService');
  }

  /** GET stats from the server */
  getUsers(): Observable<Users[]> {
    return this.http.get<Users[]>(this.config.apiEndpoint+'/admin/users', { headers })
      .pipe(
        catchError(this.handleError('getUpcomings', []))
      );
  }
}
