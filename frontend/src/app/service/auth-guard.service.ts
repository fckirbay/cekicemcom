import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  token: any;
  decoded: any;

  constructor(private router: Router) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    return new Promise((resolve, reject) => {
      if(localStorage.getItem('access_token')) {
        this.token = localStorage.getItem('access_token');
        this.decoded = jwt_decode(this.token);
        if (this.decoded.exp < (new Date().getTime() + 1) / 1000) {
          localStorage.removeItem('access_token');
          this.router.navigate(['/login']);
          return resolve(false);
        } else {
          return resolve(true);
        }
      } else {
        this.router.navigate(['/login']);
        return resolve(false);
      }
    });
  }
}